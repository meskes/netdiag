/*  curs.c is part of Statnet */
/* Statnet is protected under the GNU Public License (GPL2). */
/* Author: Jeroen Baekelandt (jeroenb@igwe.vub.ac.be)       */
#include "config.h"

#ifdef NEWCURSES_SUPP
#include <ncurses/curses.h>
#else
#ifdef NEWCURSESROOT_SUPP
#include <ncurses.h>
#else
#include <curses.h>
#endif
#endif

#include <string.h>
#include <stdarg.h>
#include "curs.h"
#include "netwatch.h"
extern int colour;

void
init_curses ()
{
  initscr ();
//  nodelay (stdscr, TRUE);
  cbreak ();
  noecho ();
}


void
cleanup_curses ()
{
  echo();
 // nodelay(stdscr,FALSE);
  nocbreak();
  endwin ();
}

void
clrportion (int y1, int x1, int y2, int x2)
{
  int i, j;

  
  j = x2 - x1;
  if (j<=0) return;
  if (y1>y2) return;
  for (i = y1; i < y2; i++)
    mvprintw (i, x1, "%*c", j, ' ');
  return;
}

void
clrscr ()
{
  int i;
//  clear ();
  attrset (col4);
  /*
  for (i = 0; i < MLINES; i++)
    mvchgat (i, 0, MCOLS, col4, 0, NULL);
*/
  bkgdset(col4);
  clear();
/*  color_set(col4,NULL); */
  border (0, 0, 0, 0, 0, 0, 0, 0);
  refresh ();
}
