/*  stat.h is part of Statnet */
/* Statnet is protected under the GNU Public License (GPL2). */
/* Author: Jeroen Baekelandt (jeroenb@igwe.vub.ac.be)       */

/* DEFINE OLDLINUX IF YOU ARE WORKING WITH A SYSTEM OLDER THAN SLACKWARE 96
   or a release which has a good /usr/include/ncurses/ncurses.h
#include <stdio.h>
#define OLDLINUX
*/


#define ETH "eth0"
#define SN_UPDATE_SECS		6	/* Number of seconds between updates. */
					/* Suggest 4 to 6 seconds minimum, as */
					/* two seconds is usually too fast to */
					/* see relationships.  CPU time use is*/
					/* mostly due to packet processing if */
					/* update is greater than 3 seconds.  */

#define SN_STATS_SECS		(6*60)	/* Number of seconds between stats updates */

#define SN_NUM_PROTOCOLS	8	/* Number of Ethernet protocols to show */
#define SN_MAX_PROTO_DESC       200	/* Number of Ethernet protocols to know */
#define SN_NUM_IP_TYPES		7	/* Number of IP protocols to show */
#define SN_NUM_TCP_PORTS	9	/* Number of TCP ports to show */
#define SN_NUM_UDP_PORTS	9	/* Number of UDP ports to show */
#define SN_NUM_SAP_TYPES	7	/* Number of IP protocols to show */
#define SN_MAX_IP_PORT		256	/* Number of IP protocols to tally */
#define SN_MAX_TCP_PORTS 	1024	/* Number of TCP ports to tally */
#define SN_MAX_UDP_PORTS 	1024	/* Number of UDP ports to tally */
#define SN_MAX_SAP		256	/* Number of 802.2 SAP to tally */
#define SN_PORT_TYPE_LEN 	20	/* Length of type labels */
#define SN_LIST_SWAP		5	/* Number of packets to require movement higher in list */

#define SN_PROT_IEEE802_3	0	/* Pseudo protocol for IEEE 802.3 */
#define SN_PROT_SLIP		162	/* Pseudo protocol for SLIP */
#define SN_PROT_PPP		163	/* Pseudo protocol for PPP */
#define SN_PROT_LOOP		164	/* Pseudo protocol for Loopback */

#include <sys/types.h>
#include "config.h"
/*
#ifdef NETINET_SUPP_socket
#include <netinet/socket.h>
#else
#include <linux/socket.h>
#endif
*/
#ifdef NETINET_SUPP_in
#include <netinet/in.h>
#else
typedef unsigned short  sa_family_t;

#include <linux/in.h>
#endif
#ifdef NETINET_SUPP_ip
#include <netinet/ip.h>
#else
#include <linux/ip.h>
#endif
#ifdef NETINET_SUPP_udp
#include <netinet/udp.h>
#else
#ifdef NETINET_SUPP_ip_udp
#include <netinet/ip_udp.h>
#else
#include <linux/udp.h>
#endif
#endif
#ifdef NETINET_SUPP_tcp
#include <netinet/tcp.h>
#else
#ifdef NETINET_SUPP_ip_tcp
#include <netinet/ip_tcp.h>
#else
#include <linux/tcp.h>
#endif
#endif
#ifdef NETINET_SUPP_if_ether
#include <netinet/if_ether.h>
#else
#include <linux/if_ether.h>
#endif


#include "core.h"
void dispdata(int errnum);
void services();
void usage(char *arg);
void clrportion(int y1, int x1, int y2, int x2);

struct registers{
	int ip_option :1;	/* Types window */
	int g :1;	/* General window */
        int prot_option :1;	/* Protocol activity */
	int at_option :1;	/* Appletalk activity */
	int tcp_option :1;	/* TCP/IP activity */
	int udp_option :1;	/* UDP activity */
	int sap_option :1;	/* SAP activity */

	/* Ethernet interface */
        int  ethercount;
        long etherbytes;

	/* PLIP interface */
	int  plipcount;
	long plipbytes;

	/* SLIP interface */
	int  slipcount;
	long slipbytes;

	/* PPP interface */
	int  pppcount;
	long pppbytes;

	/* loopback interface */
	int  loopcount;
	long loopbytes;

	int  othercount;
	long otherbytes;

	/* Appletalk types */
	int aarp;
	int rtmprd;
	int nbp;
	int atp;
	int aep;
	int rtmpreq;
	int zip;
	int adsp;

        /* IEEE802.2 protocol */
        int new_ethernet_count;

        /* unknown types */
	int unknown_type;
	int unknown_frame_type;		/* store last unknown frame code */
	int unknown_sap;		/* store last unknown sap codes */

        /* Received error count and last error code */
	int errcount;
	int errcode;			/* store last error code */

        /* Ethernet protocol types to display */
	int prot_types[SN_NUM_PROTOCOLS];

        /* IP protocol types to display */
	int IP_types[SN_NUM_IP_TYPES];

        /* TCP port numbers to display */
	int tcp_ports[SN_NUM_TCP_PORTS];

        /* UDP port numbers to display */
	int udp_ports[SN_NUM_UDP_PORTS];

        /* SAP protocol types to display */
	int SAP_types[SN_NUM_SAP_TYPES];

        };

#ifdef MAIN_LINE
#define EXTERN_DEF
#else
#define EXTERN_DEF	extern
#endif

EXTERN_DEF struct registers	regis;
EXTERN_DEF int			packet_type;
EXTERN_DEF int			frame_protocol;;
EXTERN_DEF int			rewrite_labels;
EXTERN_DEF int			redraw_screen;
EXTERN_DEF int			help_flag;
EXTERN_DEF int			temp_int;
EXTERN_DEF int			stats_countdown;
/*
EXTERN_DEF struct enet_statistics	*last_stats;
EXTERN_DEF struct enet_statistics	*now_stats;
EXTERN_DEF struct enet_statistics	*temp_stats;
EXTERN_DEF struct enet_statistics	stat_buf1;
EXTERN_DEF struct enet_statistics	stat_buf2;
*/
/* Note the +1 added to ints to allow direct reference to constant values   */
/* instead of having to subtract 1 due to array address starting from zero. */
/* Note the +1 added to char strings for the terminating NULL. */
EXTERN_DEF int	protocol_count[SN_NUM_PROTOCOLS+1];	/* Count of frames */
EXTERN_DEF int	protocol_num[SN_MAX_PROTO_DESC+1];	/* Protocol numbers */
EXTERN_DEF int	ip_protocol_count[SN_MAX_IP_PORT+1];
EXTERN_DEF int	tcp_port_count[SN_MAX_TCP_PORTS+1];
EXTERN_DEF int	udp_port_count[SN_MAX_UDP_PORTS+1];
EXTERN_DEF int	sap_count[SN_MAX_SAP+1];
EXTERN_DEF char	ip_protocol_types[SN_MAX_IP_PORT+1][SN_PORT_TYPE_LEN+1];
/*
EXTERN_DEF char	tcp_port_types[SN_MAX_TCP_PORTS+1][SN_PORT_TYPE_LEN+1];
EXTERN_DEF char	udp_port_types[SN_MAX_UDP_PORTS+1][SN_PORT_TYPE_LEN+1];
*/
EXTERN_DEF char	sap_port_types[SN_MAX_SAP+1][SN_PORT_TYPE_LEN+1];
EXTERN_DEF char	protocol_types[SN_MAX_PROTO_DESC+1][SN_PORT_TYPE_LEN+1];
#include "icmpdefs.gen"
EXTERN_DEF char icmp_types[ICMPMAX+2][SN_PORT_TYPE_LEN+1];
/* Default icmp type is at  ICMPMAX+2... */

/*   For UDP and TCP Port Info...  */
struct port_info {
	int port;
	char *name;
	struct port_info *link;
};

#define TCPHASH 1786
struct port_info *tcp_port_types[TCPHASH];

#define UDPHASH 1786
struct port_info *udp_port_types[UDPHASH];

int hashport( int port, int hash);
void initlist(struct port_info *first[], int hash);
char *searchlist(struct port_info *first[], int port, int hash);
char *servicenm( char *s, int port);


	
struct hostinfo {
   unsigned char addr[4];
   unsigned char othaddr[4];
   char name[RESOLVE_MAX];
   char ip_pr[15];
   unsigned char mac[6]; /* ETH_ALEN!!!! */
   unsigned char badmac[6]; /* ETH_ALEN!!!! */
   unsigned long badmactime;
   char macempty;
   char *servicename;   /*  replaced with a simple record pointer */
   char *oth_sn;   /*  replaced with a simple record pointer */
   int port;
   int oth_port;
   char server[26];
   char lastget[26];
   char ftpserver[26];
   unsigned long pktcntsend;
   unsigned long extpktcntsend;
   unsigned long intpktcntsend;
   unsigned long pktcntrec;
   unsigned long extpktcntrec;
   unsigned long intpktcntrec;
   time_t	tstamp;
   unsigned long sendbytes;
   unsigned long extsendbytes;
   unsigned long intsendbytes; 
   unsigned long recbytes;
   unsigned long extrecbytes;
   unsigned long intrecbytes; 
   unsigned long timebytes;
   unsigned long timelastbytes;
 /*  int disprow; */
   char telnet_in;
   char update;
   char *inpacklog;
   char *outpacklog;
   struct hostinfo *flink;
   struct hostinfo *blink;
   struct hostinfo *tflink;
   struct hostinfo *tblink;
   int plog;
   int plogactive;
   };
typedef struct hostinfo HOSTINFO;

#define DISP_TX_RC 	0
#define DISP_EXTTXRC    1
#define DISP_INTTXRC	2
#define DISP_EXTTXRCP   3
#define DISP_INTTXRCP	4
#define DISP_IPPROTO	5
#define DISP_IPPROTON   6
#define DISP_DEST1	7
#define DISP_DEST2	8
#define DISP_ACCT	9
#define DISP_MAC	10
#define DISP_BADMAC	11
#define DISP_SERVER     12
#define DISP_FTPSERVER  13
#define DISP_LASTGET    14
#define DISP_LASTINTERVAL    15
#define DISP_LASTLIVEINT    16
#define DISP_WATCH	17

#define DISP_MAX	DISP_LASTLIVEINT
#define CURSESBUG	7 

EXTERN_DEF int col1, col2, col3, col4, col5, col6;  /*  General Attributes Variables */
EXTERN_DEF int MCOLS, MLINES;

#define TELNET_PORT 23
