#!/bin/bash
VER=`cat version`
NEWVER=`./bump $VER r`
echo $NEWVER > version
mv netwatch.${VER}.lsm netwatch.${NEWVER}.lsm
sed "s/$VER/$NEWVER/g" Makefile.in > ttt
/bin/rm Makefile.in
mv ttt Makefile.in
sed "/= \"${VER}\"/s//= \"${NEWVER}\"/" netwatch.c > ttt
/bin/rm netwatch.c
mv ttt netwatch.c
cd ..
mv netwatch-$VER netwatch-$NEWVER
cd netwatch-$NEWVER
./configure
make clean
make tar
./configure
sed "s/Src/Bin/g" gh.c > ttt
/bin/rm gh.c
mv ttt gh.c
make
cp ../Makefile.netwatch.bin Makefile
sed "s/XXXX/$NEWVER/g" Makefile > ttt
/bin/rm Makefile
mv ttt Makefile
/bin/rm *.in conf* *.o *.c *.awk *.gen *.ports *.h Mak*common
/bin/rm -r IANA
make tar
cp *.lsm ..
