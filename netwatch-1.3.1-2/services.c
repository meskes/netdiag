
/*  services.c is part of Statnet */
/* Statnet is protected under the GNU Public License (GPL2). */
/* Author: 15DEC95: Scot E. Wilcoxon (sewilco@fieldday.mn.org)     */

/* This routine initializes the names for port numbers. */
/* These are from RFC 1700, "Assigned Numbers", which are from hither and yon. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "netwatch.h"

int hashport( int port, int hash)
{
	return(port % hash);
}


void initicmp()
{
	int i;
	
	for (i=0;i<=ICMPMAX;i++)
		icmp_types[i][0]=0;
	strcpy(icmp_types[ICMPMAX+1],"UNK-ICMP");
}

void initlist(struct port_info *first[], int hash)
{
	int i;
	
	for (i=0;i<hash;i++)
		first[i]=NULL;
}

char *searchlist(struct port_info *first[], int port, int hash)
{
	struct port_info *current;
	
		current = first[hashport(port,hash)];
		while(current && current->port!=port)
			current=current->link;
		if (current)
		{
			return(current->name);
		}
		else
			return(NULL);
}

void addtolist(struct port_info *first[], int port, char *name, int hash)
{
	int spot;
	struct port_info *place;	
		
	spot = hashport(port,hash);

	place = (struct port_info *)malloc(
			sizeof(*first[spot]));
	place->name = malloc(strlen(name)+1);
	strcpy(place->name,name);
	place->port = port;
	if (first[spot])
		place->link = first[spot];
	else
		place->link = NULL;
	first[spot] = place;

}


void
services ()
{
  int temp_int;

	initlist(tcp_port_types,TCPHASH);
	initlist(udp_port_types,UDPHASH);
	initicmp();


#include "tcp.gen" 
#include "udp.gen"	
#include "icmp.gen"

#if( defined(SN_MAX_IP_PORT) )
  for (temp_int = 0; temp_int <= SN_MAX_IP_PORT; temp_int++)
    {				/* Put port number as default label for all ports */
      sprintf (ip_protocol_types[temp_int], "%d:", temp_int);
    }				/* Put port number as default label for all ports */

  strncpy (ip_protocol_types[1], "ICMP:", SN_PORT_TYPE_LEN);	/* Internet Control Message       [RFC792,JBP] */
  strncpy (ip_protocol_types[2], "IGMP:", SN_PORT_TYPE_LEN);	/* Internet Group Management     [RFC1112,JBP] */
  strncpy (ip_protocol_types[3], "GGP:", SN_PORT_TYPE_LEN);	/* Gateway-to-Gateway              [RFC823,MB] */
  strncpy (ip_protocol_types[4], "IP:", SN_PORT_TYPE_LEN);	/* IP in IP (encasulation)               [JBP] */
  strncpy (ip_protocol_types[5], "ST:", SN_PORT_TYPE_LEN);	/* Stream                 [RFC1190,IEN119,JWF] */
  strncpy (ip_protocol_types[6], "TCP:", SN_PORT_TYPE_LEN);	/* Transmission Control           [RFC793,JBP] */
  strncpy (ip_protocol_types[7], "UCL:", SN_PORT_TYPE_LEN);	/* UCL                                    [PK] */
  strncpy (ip_protocol_types[8], "EGP:", SN_PORT_TYPE_LEN);	/* Exterior Gateway Protocol     [RFC888,DLM1] */
  strncpy (ip_protocol_types[9], "IGP:", SN_PORT_TYPE_LEN);	/* any private interior gateway          [JBP] */
  strncpy (ip_protocol_types[10], "BBN-RCC-MON:", SN_PORT_TYPE_LEN);	/* BBN RCC Monitoring                    [SGC] */
  strncpy (ip_protocol_types[11], "NVP-II:", SN_PORT_TYPE_LEN);		/* Network Voice Protocol         [RFC741,SC3] */
  strncpy (ip_protocol_types[12], "PUP:", SN_PORT_TYPE_LEN);	/* PUP                             [PUP,XEROX] */
  strncpy (ip_protocol_types[13], "ARGUS:", SN_PORT_TYPE_LEN);	/* ARGUS                                [RWS4] */
  strncpy (ip_protocol_types[14], "EMCON:", SN_PORT_TYPE_LEN);	/* EMCON                                 [BN7] */
  strncpy (ip_protocol_types[15], "XNET:", SN_PORT_TYPE_LEN);	/* Cross Net Debugger            [IEN158,JFH2] */
  strncpy (ip_protocol_types[16], "CHAOS:", SN_PORT_TYPE_LEN);	/* Chaos                                 [NC3] */
  strncpy (ip_protocol_types[17], "UDP:", SN_PORT_TYPE_LEN);	/* User Datagram                  [RFC768,JBP] */
  strncpy (ip_protocol_types[18], "MUX:", SN_PORT_TYPE_LEN);	/* Multiplexing                    [IEN90,JBP] */
  strncpy (ip_protocol_types[19], "DCN-MEAS:", SN_PORT_TYPE_LEN);	/* DCN Measurement Subsystems           [DLM1] */
  strncpy (ip_protocol_types[20], "HMP:", SN_PORT_TYPE_LEN);	/* Host Monitoring                [RFC869,RH6] */
  strncpy (ip_protocol_types[21], "PRM:", SN_PORT_TYPE_LEN);	/* Packet Radio Measurement              [ZSU] */
  strncpy (ip_protocol_types[22], "XNS-IDP:", SN_PORT_TYPE_LEN);	/* XEROX NS IDP               [ETHERNET,XEROX] */
  strncpy (ip_protocol_types[23], "TRUNK-1:", SN_PORT_TYPE_LEN);	/* Trunk-1                              [BWB6] */
  strncpy (ip_protocol_types[24], "TRUNK-2:", SN_PORT_TYPE_LEN);	/* Trunk-2                              [BWB6] */
  strncpy (ip_protocol_types[25], "LEAF-1:", SN_PORT_TYPE_LEN);		/* Leaf-1                               [BWB6] */
  strncpy (ip_protocol_types[26], "LEAF-2:", SN_PORT_TYPE_LEN);		/* Leaf-2                               [BWB6] */
  strncpy (ip_protocol_types[27], "RDP:", SN_PORT_TYPE_LEN);	/* Reliable Data Protocol         [RFC908,RH6] */
  strncpy (ip_protocol_types[28], "IRTP:", SN_PORT_TYPE_LEN);	/* Internet Reliable Transaction  [RFC938,TXM] */
  strncpy (ip_protocol_types[29], "ISO-TP4:", SN_PORT_TYPE_LEN);	/* ISO Transport Protocol Class 4 [RFC905,RC77] */
  strncpy (ip_protocol_types[30], "NETBLT:", SN_PORT_TYPE_LEN);		/* Bulk Data Transfer Protocol    [RFC969,DDC1] */
  strncpy (ip_protocol_types[31], "MFE-NSP:", SN_PORT_TYPE_LEN);	/* MFE Network Services Protocol  [MFENET,BCH2] */
  strncpy (ip_protocol_types[32], "MERIT-INP:", SN_PORT_TYPE_LEN);	/* MERIT Internodal Protocol             [HWB] */
  strncpy (ip_protocol_types[33], "SEP:", SN_PORT_TYPE_LEN);	/* Sequential Exchange Protocol        [JC120] */
  strncpy (ip_protocol_types[34], "PC:", SN_PORT_TYPE_LEN);	/* Third Party Connect Protocol         [SAF3] */
  strncpy (ip_protocol_types[35], "IDPR:", SN_PORT_TYPE_LEN);	/* Inter-Domain Policy Routing Protocol [MXS1] */
  strncpy (ip_protocol_types[36], "XTP:", SN_PORT_TYPE_LEN);	/* XTP                                   [GXC] */
  strncpy (ip_protocol_types[37], "DDP:", SN_PORT_TYPE_LEN);	/* Datagram Delivery Protocol            [WXC] */
  strncpy (ip_protocol_types[38], "IDPR-CMTP:", SN_PORT_TYPE_LEN);	/* IDPR Control Message Transport Proto [MXS1] */
  strncpy (ip_protocol_types[39], "TP++:", SN_PORT_TYPE_LEN);	/* TP++ Transport Protocol               [DXF] */
  strncpy (ip_protocol_types[40], "IL:", SN_PORT_TYPE_LEN);	/* IL Transport Protocol                [DXP2] */
  strncpy (ip_protocol_types[41], "SIP:", SN_PORT_TYPE_LEN);	/* Simple Internet Protocol              [SXD] */
  strncpy (ip_protocol_types[42], "SDRP:", SN_PORT_TYPE_LEN);	/* Source Demand Routing Protocol       [DXE1] */
  strncpy (ip_protocol_types[43], "SIP-SR:", SN_PORT_TYPE_LEN);		/* SIP Source Route                      [SXD] */
  strncpy (ip_protocol_types[44], "SIP-FRAG:", SN_PORT_TYPE_LEN);	/* SIP Fragment                          [SXD] */
  strncpy (ip_protocol_types[45], "IDRP:", SN_PORT_TYPE_LEN);	/* Inter-Domain Routing Protocol   [Sue Hares] */
  strncpy (ip_protocol_types[46], "RSVP:", SN_PORT_TYPE_LEN);	/* Reservation Protocol           [Bob Braden] */
  strncpy (ip_protocol_types[47], "GRE:", SN_PORT_TYPE_LEN);	/* General Routing Encapsulation     [Tony Li] */
  strncpy (ip_protocol_types[48], "MHRP:", SN_PORT_TYPE_LEN);	/* Mobile Host Routing Protocol[David Johnson] */
  strncpy (ip_protocol_types[49], "BNA:", SN_PORT_TYPE_LEN);	/* BNA                          [Gary Salamon] */
  strncpy (ip_protocol_types[50], "SIPP-ESP:", SN_PORT_TYPE_LEN);	/* SIPP Encap Security Payload [Steve Deering] */
  strncpy (ip_protocol_types[51], "SIPP-AH:", SN_PORT_TYPE_LEN);	/* SIPP Authentication Header  [Steve Deering] */
  strncpy (ip_protocol_types[52], "I-NLSP:", SN_PORT_TYPE_LEN);		/* Integrated Net Layer Security  TUBA [GLENN] */
  strncpy (ip_protocol_types[53], "SWIPE:", SN_PORT_TYPE_LEN);	/* IP with Encryption                    [JI6] */
  strncpy (ip_protocol_types[54], "NHRP:", SN_PORT_TYPE_LEN);	/* NBMA Next Hop Resolution Protocol */
  strncpy (ip_protocol_types[61], "anyhost:", SN_PORT_TYPE_LEN);	/* any host internal protocol            [JBP] */
  strncpy (ip_protocol_types[62], "CFTP:", SN_PORT_TYPE_LEN);	/* CFTP                            [CFTP,HCF2] */
  strncpy (ip_protocol_types[63], "anylan:", SN_PORT_TYPE_LEN);		/* any local network                     [JBP] */
  strncpy (ip_protocol_types[64], "SAT-EXPAK:", SN_PORT_TYPE_LEN);	/* SATNET and Backroom EXPAK             [SHB] */
  strncpy (ip_protocol_types[65], "KRYPTOLAN:", SN_PORT_TYPE_LEN);	/* Kryptolan                            [PXL1] */
  strncpy (ip_protocol_types[66], "RVD:", SN_PORT_TYPE_LEN);	/* MIT Remote Virtual Disk Protocol      [MBG] */
  strncpy (ip_protocol_types[67], "IPPC:", SN_PORT_TYPE_LEN);	/* Internet Pluribus Packet Core         [SHB] */
  strncpy (ip_protocol_types[68], "dfs:", SN_PORT_TYPE_LEN);	/* any distributed file system           [JBP] */
  strncpy (ip_protocol_types[69], "SAT-MON:", SN_PORT_TYPE_LEN);	/* SATNET Monitoring                     [SHB] */
  strncpy (ip_protocol_types[70], "VISA:", SN_PORT_TYPE_LEN);	/* VISA Protocol                        [GXT1] */
  strncpy (ip_protocol_types[71], "IPCV:", SN_PORT_TYPE_LEN);	/* Internet Packet Core Utility          [SHB] */
  strncpy (ip_protocol_types[72], "CPNX:", SN_PORT_TYPE_LEN);	/* Computer Protocol Network Executive  [DXM2] */
  strncpy (ip_protocol_types[73], "CPHB:", SN_PORT_TYPE_LEN);	/* Computer Protocol Heart Beat         [DXM2] */
  strncpy (ip_protocol_types[74], "WSN:", SN_PORT_TYPE_LEN);	/* Wang Span Network                     [VXD] */
  strncpy (ip_protocol_types[75], "PVP:", SN_PORT_TYPE_LEN);	/* Packet Video Protocol                 [SC3] */
  strncpy (ip_protocol_types[76], "BR-SAT-MON:", SN_PORT_TYPE_LEN);	/* Backroom SATNET Monitoring            [SHB] */
  strncpy (ip_protocol_types[77], "SUN-ND:", SN_PORT_TYPE_LEN);		/* SUN ND PROTOCOL-Temporary             [WM3] */
  strncpy (ip_protocol_types[78], "WB-MON:", SN_PORT_TYPE_LEN);		/* WIDEBAND Monitoring                   [SHB] */
  strncpy (ip_protocol_types[79], "WB-EXPAK:", SN_PORT_TYPE_LEN);	/* WIDEBAND EXPAK                        [SHB] */
  strncpy (ip_protocol_types[80], "ISO-IP:", SN_PORT_TYPE_LEN);		/* ISO Internet Protocol                 [MTR] */
  strncpy (ip_protocol_types[81], "VMTP:", SN_PORT_TYPE_LEN);	/* VMTP                                 [DRC3] */
  strncpy (ip_protocol_types[82], "SECURE-VMTP:", SN_PORT_TYPE_LEN);	/* SECURE-VMTP                          [DRC3] */
  strncpy (ip_protocol_types[83], "VINES:", SN_PORT_TYPE_LEN);	/* VINES                                 [BXH] */
  strncpy (ip_protocol_types[84], "TTP:", SN_PORT_TYPE_LEN);	/* TTP                                   [JXS] */
  strncpy (ip_protocol_types[85], "NSFNET-IGP:", SN_PORT_TYPE_LEN);	/* NSFNET-IGP                            [HWB] */
  strncpy (ip_protocol_types[86], "DGP:", SN_PORT_TYPE_LEN);	/* Dissimilar Gateway Protocol     [DGP,ML109] */
  strncpy (ip_protocol_types[87], "TCF:", SN_PORT_TYPE_LEN);	/* TCF                                  [GAL5] */
  strncpy (ip_protocol_types[88], "IGRP:", SN_PORT_TYPE_LEN);	/* IGRP                            [CISCO,GXS] */
  strncpy (ip_protocol_types[89], "OSPFIGP:", SN_PORT_TYPE_LEN);	/* OSPFIGP                      [RFC1583,JTM4] */
  strncpy (ip_protocol_types[90], "Sprite-RPC:", SN_PORT_TYPE_LEN);	/* Sprite RPC Protocol            [SPRITE,BXW] */
  strncpy (ip_protocol_types[91], "LARP:", SN_PORT_TYPE_LEN);	/* Locus Address Resolution Protocol     [BXH] */
  strncpy (ip_protocol_types[92], "MTP:", SN_PORT_TYPE_LEN);	/* Multicast Transport Protocol          [SXA] */
  strncpy (ip_protocol_types[93], "AX.25:", SN_PORT_TYPE_LEN);	/* AX.25 Frames                         [BK29] */
  strncpy (ip_protocol_types[94], "IPIP:", SN_PORT_TYPE_LEN);	/* IP-within-IP Encapsulation Protocol   [JI6] */
  strncpy (ip_protocol_types[95], "MICP:", SN_PORT_TYPE_LEN);	/* Mobile Internetworking Control Pro.   [JI6] */
  strncpy (ip_protocol_types[96], "SCC-SP:", SN_PORT_TYPE_LEN);		/* Semaphore Communications Sec. Pro.    [HXH] */
  strncpy (ip_protocol_types[97], "ETHERIP:", SN_PORT_TYPE_LEN);	/* Ethernet-within-IP Encapsulation     [RXH1] */
  strncpy (ip_protocol_types[98], "ENCAP:", SN_PORT_TYPE_LEN);	/* Encapsulation Header         [RFC1241,RXB3] */
  strncpy (ip_protocol_types[99], "encrypt:", SN_PORT_TYPE_LEN);	/* any private encryption scheme         [JBP] */
  strncpy (ip_protocol_types[100], "GMTP:", SN_PORT_TYPE_LEN);	/* GMTP                                 [RXB5] */

#endif


#if( defined(SN_MAX_SAP) )
#if( SN_MAX_SAP >= 256 )
  for (temp_int = 0; temp_int < SN_MAX_SAP + 1; temp_int++)
    {				/* Put number as default label for all */
      sprintf (sap_port_types[temp_int], "0x%02X:", temp_int);
    }				/* Put number as default label for all */

  /* The SAP numbers came from someplace on the Web, called */
  /* cisco-lsap-list.txt */
  /* ieee-lsap-list.txt */

  /* The low bit is a group address flag, and odd-numbered addresses */
  /* have been added to allow clearer labeling of what's seen. */

  /* IEEE - Administered LSAPs */
  strncpy (sap_port_types[0x00], "Null:", SN_PORT_TYPE_LEN);	/* Null LSAP */
  strncpy (sap_port_types[0x01], "Null grp:", SN_PORT_TYPE_LEN);
  strncpy (sap_port_types[0x02], "LLC Mgt:", SN_PORT_TYPE_LEN);		/* Individual LLC Sublayer Mgmt Function */
  strncpy (sap_port_types[0x03], "LLCgMgt:", SN_PORT_TYPE_LEN);		/* Group LLC Sublayer Mgmt Function */
  strncpy (sap_port_types[0x06], "IP:", SN_PORT_TYPE_LEN);	/* ARPANET Internet Protocol (IP) */
  strncpy (sap_port_types[0x07], "IP grp:", SN_PORT_TYPE_LEN);	/* ARPANET Internet Protocol (IP) */
  strncpy (sap_port_types[0x0E], "PROWAY Mgt:", SN_PORT_TYPE_LEN);	/* PROWAY (IEC955) Network Mgmt & Initialization */
  strncpy (sap_port_types[0x0F], "PROWAYgMgt:", SN_PORT_TYPE_LEN);	/* PROWAY (IEC955) Network Mgmt & Initialization */
  strncpy (sap_port_types[0x42], "SpanTree:", SN_PORT_TYPE_LEN);	/* IEEE 802.1 Bridge Spanning Tree Protocol */
  strncpy (sap_port_types[0x43], "SpanT grp:", SN_PORT_TYPE_LEN);	/* IEEE 802.1 Bridge Spanning Tree Protocol */
  strncpy (sap_port_types[0x4E], "ManMsg:", SN_PORT_TYPE_LEN);	/* EIA RS-511 Manufacturing Message Service */
  strncpy (sap_port_types[0x4F], "ManMsg grp:", SN_PORT_TYPE_LEN);	/* EIA RS-511 Manufacturing Message Service */
  strncpy (sap_port_types[0x7E], "X.25:", SN_PORT_TYPE_LEN);	/* ISO 8208 (X.25 over IEEE 802.2 Type 2 LLC) */
  strncpy (sap_port_types[0x7F], "X.25 grp:", SN_PORT_TYPE_LEN);	/* ISO 8208 (X.25 over IEEE 802.2 Type 2 LLC) */
  strncpy (sap_port_types[0x8E], "POWAY Stn:", SN_PORT_TYPE_LEN);	/* PROWAY (IEC955) Active Station List Maintenance */
  strncpy (sap_port_types[0x8F], "POWAYgStn:", SN_PORT_TYPE_LEN);	/* PROWAY (IEC955) Active Station List Maintenance */
  strncpy (sap_port_types[0xAA], "SNAP:", SN_PORT_TYPE_LEN);	/* Sub-Network Access Protocol (SNAP) */
  strncpy (sap_port_types[0xAB], "SNAP grp:", SN_PORT_TYPE_LEN);	/* Sub-Network Access Protocol (SNAP) */
  strncpy (sap_port_types[0xFE], "ISO Net:", SN_PORT_TYPE_LEN);		/* ISO Network Layer Protocol */
  strncpy (sap_port_types[0xFF], "Global:", SN_PORT_TYPE_LEN);	/* Global LSAP */

  /* Manufacturer-Implemented LSAPs */
  strncpy (sap_port_types[0x04], "SNA:", SN_PORT_TYPE_LEN);	/* IBM SNA Path Control (individual) */
  strncpy (sap_port_types[0x05], "SNA grp:", SN_PORT_TYPE_LEN);		/* IBM SNA Path Control (group) */
  strncpy (sap_port_types[0x18], "TI:", SN_PORT_TYPE_LEN);	/* Texas Instruments */
  strncpy (sap_port_types[0x19], "TI grp:", SN_PORT_TYPE_LEN);	/* Texas Instruments */
  strncpy (sap_port_types[0x80], "XNS:", SN_PORT_TYPE_LEN);	/* Xerox Network Systems (XNS) */
  strncpy (sap_port_types[0x81], "XNS grp:", SN_PORT_TYPE_LEN);		/* Xerox Network Systems (XNS) */
  strncpy (sap_port_types[0x86], "Nestar:", SN_PORT_TYPE_LEN);	/* Nestar */
  strncpy (sap_port_types[0x87], "Nestar grp:", SN_PORT_TYPE_LEN);	/* Nestar */
  strncpy (sap_port_types[0x98], "ARP:", SN_PORT_TYPE_LEN);	/* ARPANET Address Resolution Protocol (ARP) */
  strncpy (sap_port_types[0x99], "ARP grp:", SN_PORT_TYPE_LEN);		/* ARPANET Address Resolution Protocol (ARP) */
  strncpy (sap_port_types[0xBC], "VINES:", SN_PORT_TYPE_LEN);	/* Banyan VINES */
  strncpy (sap_port_types[0xBD], "VINES grp:", SN_PORT_TYPE_LEN);	/* Banyan VINES */
  strncpy (sap_port_types[0xE0], "NetWare:", SN_PORT_TYPE_LEN);		/* Novell Netware */
  strncpy (sap_port_types[0xE1], "NetW grp:", SN_PORT_TYPE_LEN);	/* Novell Netware */
  strncpy (sap_port_types[0xF0], "NetBIOS:", SN_PORT_TYPE_LEN);		/* IBM NetBIOS */
  strncpy (sap_port_types[0xF1], "NetB grp:", SN_PORT_TYPE_LEN);	/* IBM NetBIOS */
  strncpy (sap_port_types[0xF4], "LAN Mgt:", SN_PORT_TYPE_LEN);		/* IBM LAN Management (individual) */
  strncpy (sap_port_types[0xF5], "LANgMgt:", SN_PORT_TYPE_LEN);		/* IBM LAN Management (group) */
  strncpy (sap_port_types[0xF8], "RPL:", SN_PORT_TYPE_LEN);	/* IBM Remote Program Load (RPL) */
  strncpy (sap_port_types[0xF9], "RPL grp:", SN_PORT_TYPE_LEN);		/* IBM Remote Program Load (RPL) */
  strncpy (sap_port_types[0xFA], "UB:", SN_PORT_TYPE_LEN);	/* Ungermann-Bass */
  strncpy (sap_port_types[0xFB], "UB grp:", SN_PORT_TYPE_LEN);	/* Ungermann-Bass */

  /* further mysteries... */
  strncpy (sap_port_types[0x0C], "xSNA:", SN_PORT_TYPE_LEN);	/* SNA */
  strncpy (sap_port_types[0x0D], "xSNA grp:", SN_PORT_TYPE_LEN);	/* SNA */
  strncpy (sap_port_types[0x10], "xNetWare:", SN_PORT_TYPE_LEN);	/* Netware */
  strncpy (sap_port_types[0x11], "xNetW grp:", SN_PORT_TYPE_LEN);	/* Netware */
  strncpy (sap_port_types[0xFC], "xRPL:", SN_PORT_TYPE_LEN);	/* RPL */
  strncpy (sap_port_types[0xFD], "xRPL grp:", SN_PORT_TYPE_LEN);	/* RPL */
#endif
#endif

#if( defined(SN_MAX_PROTO_DESC) )
#if( SN_MAX_PROTO_DESC >= 200 )
  for (temp_int = 0; temp_int < SN_MAX_PROTO_DESC + 1; temp_int++)
    {				/* Put number as default label for all */
      protocol_num[temp_int] = 0;
      sprintf (protocol_types[temp_int], "%d", temp_int);
    }				/* Put number as default label for all */

  protocol_num[0] = 0x0000;
  strncpy (protocol_types[0], "IEEE802.3", SN_PORT_TYPE_LEN);
  protocol_num[1] = 0x0101;
  strncpy (protocol_types[1], "Experimental", SN_PORT_TYPE_LEN);
  protocol_num[2] = 0x0200;
  strncpy (protocol_types[2], "XEROX PUP", SN_PORT_TYPE_LEN);
  protocol_num[3] = 0x0201;
  strncpy (protocol_types[3], "PUP Addr Trans", SN_PORT_TYPE_LEN);
  protocol_num[4] = 0x0400;
  strncpy (protocol_types[4], "Nixdorf", SN_PORT_TYPE_LEN);
  protocol_num[5] = 0x0600;
  strncpy (protocol_types[5], "XEROX NS IDP", SN_PORT_TYPE_LEN);
  protocol_num[6] = 0x0660;
  strncpy (protocol_types[6], "DLOG", SN_PORT_TYPE_LEN);
  protocol_num[7] = 0x0661;
  strncpy (protocol_types[7], "DLOG", SN_PORT_TYPE_LEN);
  protocol_num[8] = 0x0800;
  strncpy (protocol_types[8], "Ethernet", SN_PORT_TYPE_LEN);
  protocol_num[9] = 0x0801;
  strncpy (protocol_types[9], "X.75", SN_PORT_TYPE_LEN);
  protocol_num[10] = 0x0802;
  strncpy (protocol_types[10], "NBS", SN_PORT_TYPE_LEN);
  protocol_num[11] = 0x0803;
  strncpy (protocol_types[11], "ECMA", SN_PORT_TYPE_LEN);
  protocol_num[12] = 0x0804;
  strncpy (protocol_types[12], "Chaosnet", SN_PORT_TYPE_LEN);
  protocol_num[13] = 0x0805;
  strncpy (protocol_types[13], "X.25", SN_PORT_TYPE_LEN);
  protocol_num[14] = 0x0806;
  strncpy (protocol_types[14], "ARP", SN_PORT_TYPE_LEN);
  protocol_num[15] = 0x0807;
  strncpy (protocol_types[15], "XNS Compat", SN_PORT_TYPE_LEN);
  protocol_num[16] = 0x081C;
  strncpy (protocol_types[16], "Symbolics", SN_PORT_TYPE_LEN);
  protocol_num[17] = 0x0888;
  strncpy (protocol_types[17], "Xyplex", SN_PORT_TYPE_LEN);
  protocol_num[18] = 0x0900;
  strncpy (protocol_types[18], "UB netdebug", SN_PORT_TYPE_LEN);
  protocol_num[19] = 0x0A00;
  strncpy (protocol_types[19], "IEEE802.3 PUP", SN_PORT_TYPE_LEN);
  protocol_num[20] = 0x0A01;
  strncpy (protocol_types[20], "PUP Addr Trans", SN_PORT_TYPE_LEN);
  protocol_num[21] = 0x0BAD;
  strncpy (protocol_types[21], "VINES", SN_PORT_TYPE_LEN);
  protocol_num[22] = 0x1000;
  strncpy (protocol_types[22], "Berk Trailer neg", SN_PORT_TYPE_LEN);
  protocol_num[23] = 0x1001;
  strncpy (protocol_types[23], "Berk Trailer encap/IP", SN_PORT_TYPE_LEN);
  protocol_num[24] = 0x1600;
  strncpy (protocol_types[24], "Valid Sys", SN_PORT_TYPE_LEN);
  protocol_num[25] = 0x4242;
  strncpy (protocol_types[25], "PCS Basic Block Protocol", SN_PORT_TYPE_LEN);
  protocol_num[26] = 0x5208;
  strncpy (protocol_types[26], "BBN Simnet", SN_PORT_TYPE_LEN);
  protocol_num[27] = 0x6000;
  strncpy (protocol_types[27], "DEC Unassigned", SN_PORT_TYPE_LEN);
  protocol_num[28] = 0x6001;
  strncpy (protocol_types[28], "DEC MOP Dump/Load", SN_PORT_TYPE_LEN);
  protocol_num[29] = 0x6002;
  strncpy (protocol_types[29], "DEC MOP Remote Console", SN_PORT_TYPE_LEN);
  protocol_num[30] = 0x6003;
  strncpy (protocol_types[30], "DEC DECNET Phase IV Route", SN_PORT_TYPE_LEN);
  protocol_num[31] = 0x6004;
  strncpy (protocol_types[31], "DEC LAT", SN_PORT_TYPE_LEN);
  protocol_num[32] = 0x6005;
  strncpy (protocol_types[32], "DEC Diag", SN_PORT_TYPE_LEN);
  protocol_num[33] = 0x6006;
  strncpy (protocol_types[33], "DEC Customer", SN_PORT_TYPE_LEN);
  protocol_num[34] = 0x6007;
  strncpy (protocol_types[34], "DEC LAVC, SCA", SN_PORT_TYPE_LEN);
  protocol_num[35] = 0x6008;
  strncpy (protocol_types[35], "DEC Unassigned", SN_PORT_TYPE_LEN);
  protocol_num[36] = 0x6010;
  strncpy (protocol_types[36], "3Com", SN_PORT_TYPE_LEN);
  protocol_num[37] = 0x7000;
  strncpy (protocol_types[37], "UB downld", SN_PORT_TYPE_LEN);
  protocol_num[38] = 0x7002;
  strncpy (protocol_types[38], "UB dia/loop", SN_PORT_TYPE_LEN);
  protocol_num[39] = 0x7020;
  strncpy (protocol_types[39], "LRT", SN_PORT_TYPE_LEN);
  protocol_num[40] = 0x7030;
  strncpy (protocol_types[40], "Proteon", SN_PORT_TYPE_LEN);
  protocol_num[41] = 0x7034;
  strncpy (protocol_types[41], "Cabletron", SN_PORT_TYPE_LEN);
  protocol_num[42] = 0x8003;
  strncpy (protocol_types[42], "Cronus VLN", SN_PORT_TYPE_LEN);
  protocol_num[43] = 0x8004;
  strncpy (protocol_types[43], "Cronus Direct", SN_PORT_TYPE_LEN);
  protocol_num[44] = 0x8005;
  strncpy (protocol_types[44], "HP Probe", SN_PORT_TYPE_LEN);
  protocol_num[45] = 0x8006;
  strncpy (protocol_types[45], "Nestar", SN_PORT_TYPE_LEN);
  protocol_num[46] = 0x8008;
  strncpy (protocol_types[46], "AT&T", SN_PORT_TYPE_LEN);
  protocol_num[47] = 0x8010;
  strncpy (protocol_types[47], "Excelan", SN_PORT_TYPE_LEN);
  protocol_num[48] = 0x8013;
  strncpy (protocol_types[48], "SGI diagnostics", SN_PORT_TYPE_LEN);
  protocol_num[49] = 0x8014;
  strncpy (protocol_types[49], "SGI net games", SN_PORT_TYPE_LEN);
  protocol_num[50] = 0x8015;
  strncpy (protocol_types[50], "SGI reserved", SN_PORT_TYPE_LEN);
  protocol_num[51] = 0x8016;
  strncpy (protocol_types[51], "SGI bounce server", SN_PORT_TYPE_LEN);
  protocol_num[52] = 0x8019;
  strncpy (protocol_types[52], "Apollo", SN_PORT_TYPE_LEN);
  protocol_num[53] = 0x802E;
  strncpy (protocol_types[53], "Tymshare", SN_PORT_TYPE_LEN);
  protocol_num[54] = 0x802F;
  strncpy (protocol_types[54], "Tigan", SN_PORT_TYPE_LEN);
  protocol_num[55] = 0x8035;
  strncpy (protocol_types[55], "RARP", SN_PORT_TYPE_LEN);
  protocol_num[56] = 0x8036;
  strncpy (protocol_types[56], "Aeonic Systems", SN_PORT_TYPE_LEN);
  protocol_num[57] = 0x8038;
  strncpy (protocol_types[57], "DEC LANBridge", SN_PORT_TYPE_LEN);
  protocol_num[58] = 0x8039;
  strncpy (protocol_types[58], "DEC Unassigned", SN_PORT_TYPE_LEN);
  protocol_num[59] = 0x803D;
  strncpy (protocol_types[59], "DEC Encryption", SN_PORT_TYPE_LEN);
  protocol_num[60] = 0x803E;
  strncpy (protocol_types[60], "DEC Unassigned", SN_PORT_TYPE_LEN);
  protocol_num[61] = 0x803F;
  strncpy (protocol_types[61], "DEC LAN Monitor", SN_PORT_TYPE_LEN);
  protocol_num[62] = 0x8040;
  strncpy (protocol_types[62], "DEC Unassigned", SN_PORT_TYPE_LEN);
  protocol_num[63] = 0x8044;
  strncpy (protocol_types[63], "Planning Research", SN_PORT_TYPE_LEN);
  protocol_num[64] = 0x8046;
  strncpy (protocol_types[64], "AT&T", SN_PORT_TYPE_LEN);
  protocol_num[65] = 0x8047;
  strncpy (protocol_types[65], "AT&T", SN_PORT_TYPE_LEN);
  protocol_num[66] = 0x8049;
  strncpy (protocol_types[66], "ExperData", SN_PORT_TYPE_LEN);
  protocol_num[67] = 0x805B;
  strncpy (protocol_types[67], "Stanford V Kernel exp.", SN_PORT_TYPE_LEN);
  protocol_num[68] = 0x805C;
  strncpy (protocol_types[68], "Stanford V Kernel prod.", SN_PORT_TYPE_LEN);
  protocol_num[69] = 0x805D;
  strncpy (protocol_types[69], "Evans & Sutherland", SN_PORT_TYPE_LEN);
  protocol_num[70] = 0x8060;
  strncpy (protocol_types[70], "Little Machines", SN_PORT_TYPE_LEN);
  protocol_num[71] = 0x8062;
  strncpy (protocol_types[71], "Counterpoint", SN_PORT_TYPE_LEN);
  protocol_num[72] = 0x8065;
  strncpy (protocol_types[72], "Univ. of Mass. @ Amherst", SN_PORT_TYPE_LEN);
  protocol_num[73] = 0x8066;
  strncpy (protocol_types[73], "Univ. of Mass. @ Amherst", SN_PORT_TYPE_LEN);
  protocol_num[74] = 0x8067;
  strncpy (protocol_types[74], "Veeco Integrated Auto.", SN_PORT_TYPE_LEN);
  protocol_num[75] = 0x8068;
  strncpy (protocol_types[75], "General Dynamics", SN_PORT_TYPE_LEN);
  protocol_num[76] = 0x8069;
  strncpy (protocol_types[76], "AT&T", SN_PORT_TYPE_LEN);
  protocol_num[77] = 0x806A;
  strncpy (protocol_types[77], "Autophon", SN_PORT_TYPE_LEN);
  protocol_num[78] = 0x806C;
  strncpy (protocol_types[78], "ComDesign", SN_PORT_TYPE_LEN);
  protocol_num[79] = 0x806D;
  strncpy (protocol_types[79], "Computgraphic", SN_PORT_TYPE_LEN);
  protocol_num[80] = 0x806E;
  strncpy (protocol_types[80], "Landmark Graphics", SN_PORT_TYPE_LEN);
  protocol_num[81] = 0x807A;
  strncpy (protocol_types[81], "Matra", SN_PORT_TYPE_LEN);
  protocol_num[82] = 0x807B;
  strncpy (protocol_types[82], "Dansk Data Elektronik", SN_PORT_TYPE_LEN);
  protocol_num[83] = 0x807C;
  strncpy (protocol_types[83], "Merit Internodal", SN_PORT_TYPE_LEN);
  protocol_num[84] = 0x807D;
  strncpy (protocol_types[84], "Vitalink Communications", SN_PORT_TYPE_LEN);
  protocol_num[85] = 0x8080;
  strncpy (protocol_types[85], "Vitalink TransLAN III", SN_PORT_TYPE_LEN);
  protocol_num[86] = 0x8081;
  strncpy (protocol_types[86], "Counterpoint", SN_PORT_TYPE_LEN);
  protocol_num[87] = 0x809B;
  strncpy (protocol_types[87], "Appletalk", SN_PORT_TYPE_LEN);
  protocol_num[88] = 0x809C;
  strncpy (protocol_types[88], "Datability", SN_PORT_TYPE_LEN);
  protocol_num[89] = 0x809F;
  strncpy (protocol_types[89], "Spider Systems", SN_PORT_TYPE_LEN);
  protocol_num[90] = 0x80A3;
  strncpy (protocol_types[90], "Nixdorf2", SN_PORT_TYPE_LEN);
  protocol_num[91] = 0x80A4;
  strncpy (protocol_types[91], "Siemens Gammasonics", SN_PORT_TYPE_LEN);
  protocol_num[92] = 0x80C0;
  strncpy (protocol_types[92], "DCA Data Exchange Cluster", SN_PORT_TYPE_LEN);
  protocol_num[93] = 0x80C4;
  strncpy (protocol_types[93], "Banyan", SN_PORT_TYPE_LEN);
  protocol_num[94] = 0x80C5;
  strncpy (protocol_types[94], "Banyan", SN_PORT_TYPE_LEN);
  protocol_num[95] = 0x80C6;
  strncpy (protocol_types[95], "Pacer Software", SN_PORT_TYPE_LEN);
  protocol_num[96] = 0x80C7;
  strncpy (protocol_types[96], "Applitek", SN_PORT_TYPE_LEN);
  protocol_num[97] = 0x80C8;
  strncpy (protocol_types[97], "Intergraph", SN_PORT_TYPE_LEN);
  protocol_num[98] = 0x80CD;
  strncpy (protocol_types[98], "Harris", SN_PORT_TYPE_LEN);
  protocol_num[99] = 0x80CF;
  strncpy (protocol_types[99], "Taylor Instrument", SN_PORT_TYPE_LEN);
  protocol_num[100] = 0x80D3;
  strncpy (protocol_types[100], "Rosemount Corporation", SN_PORT_TYPE_LEN);
  protocol_num[101] = 0x80D5;
  strncpy (protocol_types[101], "SNA/Ether", SN_PORT_TYPE_LEN);
  protocol_num[102] = 0x80DD;
  strncpy (protocol_types[102], "Varian Associates", SN_PORT_TYPE_LEN);
  protocol_num[103] = 0x80DE;
  strncpy (protocol_types[103], "Integrated Solutions TRFS", SN_PORT_TYPE_LEN);
  protocol_num[104] = 0x80E0;
  strncpy (protocol_types[104], "Allen-Bradley", SN_PORT_TYPE_LEN);
  protocol_num[105] = 0x80E4;
  strncpy (protocol_types[105], "Datability", SN_PORT_TYPE_LEN);
  protocol_num[106] = 0x80F2;
  strncpy (protocol_types[106], "Retix", SN_PORT_TYPE_LEN);
  protocol_num[107] = 0x80F3;
  strncpy (protocol_types[107], "AppleTalk AARP (Kinetics)", SN_PORT_TYPE_LEN);
  protocol_num[108] = 0x80F4;
  strncpy (protocol_types[108], "Kinetics", SN_PORT_TYPE_LEN);
  protocol_num[109] = 0x80F7;
  strncpy (protocol_types[109], "Apollo", SN_PORT_TYPE_LEN);
  protocol_num[110] = 0x80FF;
  strncpy (protocol_types[110], "Wellfleet", SN_PORT_TYPE_LEN);
  protocol_num[111] = 0x8107;
  strncpy (protocol_types[111], "Symbolics Private", SN_PORT_TYPE_LEN);
  protocol_num[112] = 0x8130;
  strncpy (protocol_types[112], "Hayes", SN_PORT_TYPE_LEN);
  protocol_num[113] = 0x8131;
  strncpy (protocol_types[113], "VG Laboratory Systems", SN_PORT_TYPE_LEN);
  protocol_num[114] = 0x8132;
  strncpy (protocol_types[114], "Bridge Communications", SN_PORT_TYPE_LEN);
  protocol_num[115] = 0x8137;
  strncpy (protocol_types[115], "Novell", SN_PORT_TYPE_LEN);
  protocol_num[116] = 0x8139;
  strncpy (protocol_types[116], "KTI", SN_PORT_TYPE_LEN);
  protocol_num[117] = 0x8148;
  strncpy (protocol_types[117], "Logicraft", SN_PORT_TYPE_LEN);
  protocol_num[118] = 0x8149;
  strncpy (protocol_types[118], "NCD", SN_PORT_TYPE_LEN);
  protocol_num[119] = 0x814A;
  strncpy (protocol_types[119], "Alpha Micro", SN_PORT_TYPE_LEN);
  protocol_num[120] = 0x814C;
  strncpy (protocol_types[120], "SNMP", SN_PORT_TYPE_LEN);
  protocol_num[121] = 0x814D;
  strncpy (protocol_types[121], "BIIN", SN_PORT_TYPE_LEN);
  protocol_num[122] = 0x814E;
  strncpy (protocol_types[122], "BIIN", SN_PORT_TYPE_LEN);
  protocol_num[123] = 0x814F;
  strncpy (protocol_types[123], "Technically Elite Concept", SN_PORT_TYPE_LEN);
  protocol_num[124] = 0x8150;
  strncpy (protocol_types[124], "Rational", SN_PORT_TYPE_LEN);
  protocol_num[125] = 0x8151;
  strncpy (protocol_types[125], "Qualcomm", SN_PORT_TYPE_LEN);
  protocol_num[126] = 0x815C;
  strncpy (protocol_types[126], "Computer Protocol Pty", SN_PORT_TYPE_LEN);
  protocol_num[127] = 0x8164;
  strncpy (protocol_types[127], "Charles River", SN_PORT_TYPE_LEN);
  protocol_num[128] = 0x817D;
  strncpy (protocol_types[128], "Protocol Engines", SN_PORT_TYPE_LEN);
  protocol_num[129] = 0x818D;
  strncpy (protocol_types[129], "Motorola", SN_PORT_TYPE_LEN);
  protocol_num[130] = 0x819A;
  strncpy (protocol_types[130], "Qualcomm", SN_PORT_TYPE_LEN);
  protocol_num[131] = 0x81A4;
  strncpy (protocol_types[131], "ARAI Bunkichi", SN_PORT_TYPE_LEN);
  protocol_num[132] = 0x81A5;
  strncpy (protocol_types[132], "RAD Network Devices", SN_PORT_TYPE_LEN);
  protocol_num[133] = 0x81B7;
  strncpy (protocol_types[133], "Xyplex", SN_PORT_TYPE_LEN);
  protocol_num[134] = 0x81CC;
  strncpy (protocol_types[134], "Apricot", SN_PORT_TYPE_LEN);
  protocol_num[135] = 0x81D6;
  strncpy (protocol_types[135], "Artisoft", SN_PORT_TYPE_LEN);
  protocol_num[136] = 0x81E6;
  strncpy (protocol_types[136], "Polygon", SN_PORT_TYPE_LEN);
  protocol_num[137] = 0x81F0;
  strncpy (protocol_types[137], "Comsat Labs", SN_PORT_TYPE_LEN);
  protocol_num[138] = 0x81F3;
  strncpy (protocol_types[138], "SAIC", SN_PORT_TYPE_LEN);
  protocol_num[139] = 0x81F6;
  strncpy (protocol_types[139], "VG Analytical", SN_PORT_TYPE_LEN);
  protocol_num[140] = 0x8203;
  strncpy (protocol_types[140], "Quantum Software", SN_PORT_TYPE_LEN);
  protocol_num[141] = 0x8221;
  strncpy (protocol_types[141], "Ascom Banking", SN_PORT_TYPE_LEN);
  protocol_num[142] = 0x823E;
  strncpy (protocol_types[142], "AdvEncrypSys", SN_PORT_TYPE_LEN);
  protocol_num[143] = 0x827F;
  strncpy (protocol_types[143], "Athena Programming", SN_PORT_TYPE_LEN);
  protocol_num[144] = 0x8263;
  strncpy (protocol_types[144], "Charles River Data System", SN_PORT_TYPE_LEN);
  protocol_num[145] = 0x829A;
  strncpy (protocol_types[145], "Inst Ind Info Tech", SN_PORT_TYPE_LEN);
  protocol_num[146] = 0x829C;
  strncpy (protocol_types[146], "Taurus Controls", SN_PORT_TYPE_LEN);
  protocol_num[147] = 0x82AC;
  strncpy (protocol_types[147], "Walker Richer & Quinn", SN_PORT_TYPE_LEN);
  protocol_num[148] = 0x8694;
  strncpy (protocol_types[148], "Idea Courier", SN_PORT_TYPE_LEN);
  protocol_num[149] = 0x869E;
  strncpy (protocol_types[149], "Computer Network Tech", SN_PORT_TYPE_LEN);
  protocol_num[150] = 0x86A3;
  strncpy (protocol_types[150], "Gateway Comm", SN_PORT_TYPE_LEN);
  protocol_num[151] = 0x86DB;
  strncpy (protocol_types[151], "SECTRA", SN_PORT_TYPE_LEN);
  protocol_num[152] = 0x86DE;
  strncpy (protocol_types[152], "Delta Controls", SN_PORT_TYPE_LEN);
  protocol_num[153] = 0x86DF;
  strncpy (protocol_types[153], "ATOMIC", SN_PORT_TYPE_LEN);
  protocol_num[154] = 0x86E0;
  strncpy (protocol_types[154], "Landis & Gyr Powers", SN_PORT_TYPE_LEN);
  protocol_num[155] = 0x8700;
  strncpy (protocol_types[155], "Motorola", SN_PORT_TYPE_LEN);
  protocol_num[156] = 0x8A96;
  strncpy (protocol_types[156], "Invisible Software", SN_PORT_TYPE_LEN);
  protocol_num[157] = 0x9000;
  strncpy (protocol_types[157], "Loopback", SN_PORT_TYPE_LEN);
  protocol_num[158] = 0x9001;
  strncpy (protocol_types[158], "3Com(Bridge) XNS Sys Mgmt", SN_PORT_TYPE_LEN);
  protocol_num[159] = 0x9002;
  strncpy (protocol_types[159], "3Com(Bridge) TCP-IP Sys", SN_PORT_TYPE_LEN);
  protocol_num[160] = 0x9003;
  strncpy (protocol_types[160], "3Com(Bridge) loop detect", SN_PORT_TYPE_LEN);
  protocol_num[161] = 0xFF00;
  strncpy (protocol_types[161], "BBN VITAL-LanBridge cache", SN_PORT_TYPE_LEN);

  protocol_num[SN_PROT_IEEE802_3] = SN_PROT_IEEE802_3;
  strncpy (protocol_types[SN_PROT_IEEE802_3], "IEEE802.3", SN_PORT_TYPE_LEN);
  protocol_num[SN_PROT_SLIP] = SN_PROT_SLIP;
  strncpy (protocol_types[SN_PROT_SLIP], "SLIP", SN_PORT_TYPE_LEN);
  protocol_num[SN_PROT_PPP] = SN_PROT_PPP;
  strncpy (protocol_types[SN_PROT_PPP], "PPP", SN_PORT_TYPE_LEN);
  protocol_num[SN_PROT_LOOP] = SN_PROT_LOOP;
  strncpy (protocol_types[SN_PROT_LOOP], "Loop int", SN_PORT_TYPE_LEN);
#endif
#endif

}
