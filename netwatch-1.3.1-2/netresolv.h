/*	Special Struct's for NETWATCH to resolve INET addresses
	into INET Hostnames
*/
#define RESOLVE_MAX 256
struct resolvaddr 
{
	pid_t pid;
	u_int32_t inetaddr;
	char *where;
};

struct gotname
{
	char *where;
	char name[RESOLVE_MAX]; 
};

