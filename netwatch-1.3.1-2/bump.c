#include <stdio.h>
/*
	bump version as

	bump version 
		whatever your version... do letter increment
	bump version n
		whatever your version... upgrade sub num.... 0.6d -> 0.7
	bump version r
		whatever your version... upgrade prime num.... 0.6d -> 1.0a
	

*/
int main( int argc, char *argv[])
{
	char lastkey;
	char subnum;
	char primnum;
	static char version[180];
	int lastspot;
	int bumpprim = 0;
	int bumpsub = 0;

	if (argc<2)
	{
		printf("0.0a");
		exit(1);
	}
	
	if (argc>2 && argv[2][0]=='n')
		bumpsub = 1;
	if (argc>2 && argv[2][0]=='r')
		bumpprim = 1;

	strcpy(version,argv[1]);
	if (strlen(version)<4)
	{
		printf("0.0a");
		exit(2);
	}
	lastspot = strlen(version) -1;
	lastkey = version[lastspot];
	subnum = version[lastspot-1];
	primnum = version[lastspot-3];

	if (!bumpsub && !bumpprim && lastkey<'z')
		version[lastspot]++;
	else	
	{
		if (!bumpprim && subnum<'9')
		{
			version[lastspot]='a';
			version[lastspot-1]++;
		}
		else
		{
			version[lastspot]='a';
			version[lastspot-1]='0';
			version[lastspot-3]++;
		}
	}
	printf("%s\n",version);
	return(0);
}
