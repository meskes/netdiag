/*
 * itstime.c is part of Statnet  NOW dispdata.c for NETWATCH
 */
/*
 * Statnet is protected under the GNU Public License (GPL2). 
 * Author: Jeroen Baekelandt (jeroenb@igwe.vub.ac.be)       
 * 27DEC95: Scot E. Wilcoxon (sewilco@fieldday.mn.org)      
 * DISPDATA.C is part of NETWATCH  which like Statnet is under GPL2 
 */
#include "config.h"

#include "netwatch.h"
#include "curs.h"
#include <sys/time.h>

#ifdef NEWCURSES_SUPP
#include <ncurses/curses.h>
#else
#ifdef NEWCURSESROOT_SUPP
#include <ncurses.h>
#else
#include <curses.h>
#endif
#endif
#if NETINET_SUPP_icmp
#include <netinet/ip_icmp.h>
#else
#include <linux/icmp.h>
#endif
#ifdef NETINET_SUPP_in
#include <netinet/in.h>
#else
#include <linux/in.h>
#endif


#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include "semaphore.h"

void setupauxscr (int *xoff, int bigscreen);
void setupwatch (int bigscreen);
extern int show_help;
extern int show_watch;

extern int destroy_program;
extern Semaphore masterdo;
EXT_CREATE_LOCK(indisp);
EXT_CREATE_LOCK(inrlist);
EXT_CREATE_LOCK(inllist);
EXT_CREATE_LOCK(resolvechange);
void clearnetresolv ();
void startnetresolv ();
char *inet_ntoa ();

#define MAXREFRESH 85
#define ROUTERSTATSMAX 160
#define TOPUPDATE 30
extern long int timezone;
extern time_t wakeup_reload;
extern int wakeup_state;
extern time_t reload_timer_start;
extern int reload_timer_sec;
extern int reload_active;
extern int freezedisplay;
extern int lastfreeze;
extern int rel_hours, rel_mins;

extern int bpsflag;

#define MAXDAYROUTE 1440
extern float dayroute[MAXDAYROUTE];
extern int cur_dayroute;
extern int routersummary;
extern int rdelta;
extern int router_offset;

extern int autostatus;

extern char specconfigfile[];
extern char statsfile[];
extern char mylog[];
extern char ethdevname[];

/*
 * printf("NETMASK=%u.%u.%u.%u    LOCALADDR=%u.%u.%u.%u\n",
 * netmask[0],netmask[1],netmask[2],netmask[3],
 * localaddr[0],localaddr[1],localaddr[2],localaddr[3]); getchar();
 */
extern unsigned char netmask[];
extern unsigned char localaddr[];

extern int dupcount;
extern int isbridge;

extern int localcount;
extern int remotecount;

#define MAXSTATUS 14
#define STATUSSIZE 512
extern char status_lines[MAXSTATUS][STATUSSIZE];
extern int cur_status_line;

extern int sentclear;
extern int recresolv;
extern int timeresolv;
extern int sendresolv;
extern int errread;
extern int errioctl;

int internal_loop_count = 0;

extern FILE *fpspeclog;
extern int speclog;
extern int printtospeclog;
extern char speclogfile[];
extern int speclogext;

extern int curskeyspot;
extern int curskeyval;
extern int cursllockout;
extern int cursrlockout;
extern int curslines;
extern int curshosttrim;
extern int cursproto;
extern int cursdest;

extern int scrmid;
struct itimerval oldtm, newtm;
extern int selecthost;
extern int numselect;
extern int selectside;
extern int selectchange;
extern int selectside;
extern char progtitle[60];
extern int ignoredomain;
extern int bluedie;
extern int selprob;
extern int selcode;
extern int sREDTIME, sYELLOWTIME, sGREENTIME;

extern long int ethcnt;
extern int keycnt;
extern int probcnt;
extern int intrcnt;

extern int nokeys;

extern int topflag;
extern HOSTINFO *toplocal;
extern HOSTINFO *topremote;
extern HOSTINFO tldummy, trdummy;

#define MAGNABUFSIZE 2048
struct magrec
{
  int len;
  char dir;
  unsigned char buf[MAGNABUFSIZE];
};

#define MAGNAMAX 800
#define MAGNAOFFS 40
extern struct magrec magna[MAGNAMAX];
extern int magnacnt;
extern int magnaoffs;
extern char magnamatch[RESOLVE_MAX];
extern HOSTINFO *magnaspot;
extern u_int32_t magnakey;
extern int magnaphys;
extern int magnafirst;
extern int magnafull;
extern HOSTINFO *magnacomhost;
extern u_int32_t magnacomkey;

extern int simchange;
extern int simfwdir;
extern int simarr[8];
char *simfmt;
extern int iseth;
extern int nw_logall;
extern char nw_allname[256];
extern FILE *nw_fpall;
extern int nw_logremote;
extern char nw_remname[256];
extern FILE *nw_fprem;
extern int nw_loglocal;
extern char nw_locname[256];
extern FILE *nw_fploc;
extern int nw_logindiv;
extern char nw_indivmain[256];
extern int nw_logselect;
extern int nw_simulate;
extern char nw_simfile[256];
enum
{ SIMSEC, SIMKEY };
extern int nw_simrate;
extern int simkeyrate;
extern FILE *simfp;
extern int plogselect;
extern int plogdeselect;
extern int buf_detail;
enum
{ B_ASCII, B_HEADER, B_RAW };

extern int using_fifo;
extern int fifoval;
extern int movemagnakeyloc;
extern int movemagnakeyrem;
extern u_int32_t searchaddr;

unsigned char *ttp;

char *servicenm (char *s, int port)
{
  static char sn[80];

  if (s)
    return (s);
  sprintf (sn, "UNK %d", port);
  return (sn);
}

void makeaddr (char naddr[], char ascii[]);

void cleancol (int col, int strow, int finrow)
{
  int i;

  for (i = strow; i <= finrow; i++)
    mvaddch (i, col, ' ');
}

int bad = 0;			/* Number of BAD chars... */
void myaddstr (char *s, int size)
{
  int i = 0;

  while (*s)
  {
    if ((*s) != '\\')
    {
      addch (*s);
      i++;
      if (i == size)
	break;
    }
    else
    {
      s++;
      switch ((*s))
      {
      case '1':
	attron (col1);
	break;
      case '2':
	attron (col2);
	break;
      case '3':
	attron (col3);
	break;
      case '4':
	attron (col4);
	break;
      case '5':
	attron (col5);
	break;
      case '6':
	attron (col6);
	break;
      default:
	bad++;
      }
    }

    s++;
  }
  attron (col4);
  refresh ();
}

void print_tcp (unsigned char *dp, unsigned short len, char *s)
{
  struct tcphdr *p = (struct tcphdr *) dp;

  sprintf (s,
	   "\\3 TCP DPORT:\\2%-5d\\3 SPORT:\\2%-5d\\3 SEQ:\\2%-5d\\3 ACK:\\2%-5d\\3 WINDOW:\\2%-5d\\3 URG \\2%d",
	   ntohs (p->dest), ntohs (p->source), ntohl (p->seq),
	   ntohl (p->ack_seq), ntohs (p->window), ntohs (p->urg_ptr));
}

void print_udp (unsigned char *dp, unsigned short len, char *s)
{
  struct udphdr *p = (struct udphdr *) dp;

  sprintf (s, "\\3UDP DPORT:\\2%5d\\3 SPORT:\\2%5d", ntohs (p->dest),
	   ntohs (p->source));
}

void print_icmp (unsigned char *dp, unsigned short len, char *s)
{
  struct icmphdr *p = (struct icmphdr *) dp;

  sprintf (s,
	   "\\3 ICMP TYPE:\\2%-3d\\3 CODE:\\2%-3d\\3 ID:\\2%-5d\\3 SEQ:\\2%-5d\\3 GATEWAY:\\2%-15s \\3FRAGMTU:\\2%d ",
	   p->type, p->code, ntohs (p->un.echo.id),
	   ntohs (p->un.echo.sequence), inet_ntoa (p->un.gateway),
	   ntohs (p->un.frag.mtu));
}



void header_to_scr (char *db, int size, struct magrec *b, int offs)
{
  char *sp = (char *) b->buf;
  static char darr[20];
  char *w;
  static char sarr[20];
  struct iphdr *p;
  struct in_addr *pa;
  unsigned char *dp;
  int i;
  unsigned int left;
  int whole;
  int realfudge;

  /* if (iseth)           Oh... I store only IP packets in magna rec..
     sp += ETH_HLEN; */
  p = (struct iphdr *) sp;
  pa = (void *) &p->daddr;
  w = inet_ntoa (*pa);
  strncpy (darr, w, 16);
  pa = (void *) &p->saddr;
  w = inet_ntoa (*pa);
  strncpy (sarr, w, 16);
  sprintf (db,
	   " \\1IP DEST:\\2%-15s\\1 SRC:\\2%-15s\\1 PROTO:\\2%-2d\\1 TTL:\\2%-3d\\1 TOS:\\2%-5d\\1 IHL:\\2%-2d\\1 TOT_LEN:\\2%-5d\\4",
	   darr, sarr, p->protocol, p->ttl, p->tos, p->ihl,
	   ntohs (p->tot_len));
  dp = (unsigned char *) p + /*p->ihl */ +sizeof (struct iphdr);
  left = size /* - p->ihl */  - sizeof (struct iphdr);
  switch (p->protocol)
  {
  case 6:			/* TCP */
    print_tcp (dp, left, &db[strlen (db)]);
    break;
  case 17:			/* TCP */
    print_udp (dp, left, &db[strlen (db)]);
    break;
  case 1:			/* TCP */
    print_icmp (dp, left, &db[strlen (db)]);
    break;
  }
  whole = strlen (db);
  if (whole < offs)
    myaddstr (" ", size);
  else
  {
    /* find out how many '\\' to the left of offs
       This is THE fudge...
     */
    for (realfudge = 0, i = 0; db[i] && i < offs; i++)
    {
      if (db[i] == '\\')
	realfudge++;
    }
    realfudge <<= 1;
    realfudge = offs - realfudge;
    if (offs + size >= 1024)
      db[1023] = 0;
    myaddstr (&db[realfudge], size);

  }
}

void raw_to_scr (char *db, int size, struct magrec *b, int offs)
{
  int i;

  attron (col2);
  for (i = 0; i < size; i++)
    printw ("%02X ", b->buf[offs + i]);
  attron (col4);
}


/*  Display option controlled by Global Variable... buf_detail = 
		0 == ASCII or .
		1 == TCP/UDP/ICMP Header info
		2 == RAW HEX DATA
*/
void displaybuf (struct magrec *b, int y, int x, int recnum)
{
  static char bform[1024];
  int i, len;
  int scrmid = (MCOLS >> 1) - 8;
  char *p;
  char *pfin;
  int sz;

  if (magnafull)
  {
    scrmid = MCOLS - 8;
    x = 3;
  }
  if (b->len < 0)
  {
    move (y, x);
    printw ("%03d:%05d", recnum, 0);
    printw ("  <** EMPTY **>");
    return;
  }
  len = b->len - magnaoffs + 1;
  p = &b->buf[magnaoffs];
  move (y, x);
  printw ("%03d:%05d", recnum, b->len);
  if (b->len > 0)
  {
    if (b->dir)
    {
      attron (col1);
      addch ('>');
      attron (col4);
    }
    else
    {
      attron (col2);
      addch ('<');
      attron (col4);
    }
  }
  switch (buf_detail)
  {
  case B_ASCII:
    if (len > 0)
    {
      pfin = p + len;
      for (i = 9; p < pfin && i < scrmid; i++, p++)
      {
	if (isprint (*p))
	  addch (*p);
	else
	  addch ('.');
      }
    }
    break;
  case B_HEADER:
    sz = scrmid - 10;
    header_to_scr (bform, sz, b, magnaoffs);
    break;
  case B_RAW:
    sz = (scrmid - 10) / 3;
    raw_to_scr (bform, sz, b, magnaoffs);
    break;
  }
}

void dostatus (int sline)
{
  int i;
  char *s;

  mvhline (MLINES - 1, 1, ACS_HLINE, MCOLS - 2);
  move (MLINES - 1, 0);
  for (s = status_lines[sline], i = 0; *s && i < MCOLS; i++, s++)
  {
    if (*s == '~')
      move (MLINES - 1, i + 1);
    /* move right */
    else
      addch (*s);
  }
}

void addtlist (HOSTINFO * rec, HOSTINFO * start)
{
  HOSTINFO *current, *previous;

  previous = start;
  current = start->tflink;
  while (current != start && current->timelastbytes > rec->timebytes)
  {
    previous = current;
    current = current->tflink;
  }
  previous->tflink = rec;
  rec->tflink = current;

}

void dispdata (int errnum)
{

  int xleft = 1;		/*
				 * Offset for Local Info 
				 */
  int xright;
  int y;
  static char tmbuf[80];
  static char tmpbuf[512];
  static int refreshscr = 0;
  static int routerstats = 0;
  extern double maxburst;
  extern double absmaxburst;
  static time_t bursttime;
  static char burst[512];
  static time_t routertime;
  static unsigned long int oldrouteruse = 0;
  static unsigned long int oldrouterto = 0;
  static unsigned long int oldrouterfrom = 0;
  unsigned long int tfuse;
  unsigned long int ttuse;
  unsigned long int ttotal;
  static unsigned long int minfuse;
  static unsigned long int mintuse;
  static unsigned long int sec;
  static unsigned long int use;
  double fuse;
  extern unsigned long int routeruse;

  int lcnt = 0;
  int rcnt = 0;
  static char ttt[512];
  static char thost[256];
  static int statsonce = FALSE;
  static int disableburst = FALSE;
  int yact;
  unsigned char *pc;
  extern int help;
  extern int watch;
  extern int llockout;
  extern int rlockout;
  extern int lydisp;
  extern int rydisp;
  extern int localkey;
  extern int localupdate;
  extern int remoteupdate;
  extern int refreshloc;
  extern int refreshrem;
  extern int poschange;
  extern int disprouterstats;
  extern unsigned long routerto;
  extern unsigned long routerfrom;
  extern unsigned long routeruse;
  static unsigned long orouterto = 0;
  static unsigned long orouterfrom = 0;
  int localbypass = FALSE;
  int remotebypass = FALSE;
  time_t mnew;
  static time_t oldmnew = 0L;
  time_t rs;
  extern time_t starttime;
  extern int dispopt;		/*
				 * TXRC IPPROTO OTHER 
				 */
  int updaterest = 0;
  int xoff;
  int ii, ij;
  int dispsize, stmag;
  struct tm *ptm;
  extern time_t actstart;
  time_t colred, colyel, colgreen, testt;
  extern HOSTINFO *lfirst;
  extern HOSTINFO *rfirst;
  HOSTINFO *current;
  HOSTINFO *pwork;
  char *tmpservice;
  static char emptystr[] = "";
  static char simstr[20];
  static time_t toptime = 0L;
  int dotop = 0;
  int topupdate = FALSE;
  int oldk;
  int yh, ybase, maxlines, num_rsum, ic;
  static float rsum[40];
  static int laststatsonce = -1;
  int nspot;
  int step5;
  int width;
  long int ntop=0;
  int val;


  do
  {
   LOCK(indisp);
    if (nw_simulate)
      mnew = actstart;
    else
      mnew = time (0);
    if (mnew > toptime)
    {
      topupdate = TRUE;
      ntop++;
      toptime = mnew + TOPUPDATE;
      oldmnew = mnew;
      if (topflag)
	dotop = TRUE;
    }
    rs = mnew - oldmnew;
    if (printtospeclog && speclog)
    {
      strcpy (tmpbuf, ctime (&mnew));
      fprintf (fpspeclog, "\nStatistics\tfrom %s\t\t  to %s\n\n",
	       ctime (&starttime), tmpbuf);
    }
    if (show_watch)
    {
	setupwatch(magnafull);
	show_watch = FALSE;
    }
    if (show_help)
    {
	setuphelp();
	show_help = FALSE;
    }
    localbypass = (!localkey && (watch || help));
    remotebypass = (localkey && (watch || help));
    colred = mnew - sREDTIME;	/*
				 * 1 min 
				 */
    colyel = mnew - sYELLOWTIME;	/*
					 * 5 min 
					 */
    colgreen = mnew - sGREENTIME;	/*
					 * 30 min 
					 */
    scrmid = MCOLS >> 1;
    xright = scrmid + 1;
    /* mvprintw(0,12,"REC=%d TM=%d IO=%d RD=%d SND=%d",recresolv,timeresolv,
       errioctl, errread, sendresolv); */



    if (!llockout && !rlockout && wakeup_state && wakeup_reload < mnew)
    {
      wakeup_state = FALSE;
      reload_active = TRUE;
    }


    if (reload_active)
    {
      if (!llockout && !rlockout
	  && mnew >= reload_timer_start + reload_timer_sec)
      {
	reload_timer_start = mnew;
	reload_active++;
/*		mvprintw(24,5,"RELOAD=%03d  SENTCLEAR=%d",reload_active,sentclear);
		refresh(); */
	nokeys = TRUE;
	//mvprintw (0, 60, "NO KEYS");
	//refresh ();
	clearnetresolv ();
	dokeyin ('l');
	dokeyin ('c');
	oldk = localkey;
	localkey = FALSE;
	dokeyin ('n');
	/* mvprintw(0,12,"REC=%d TM=%d IO=%d RD=%d SND=%d",recresolv,timeresolv,
	   errioctl, errread, sendresolv); */
	recresolv = timeresolv = errioctl = errread = sendresolv = 0;
	ethcnt = 0;		/* MASTER COUNT IS ZEROED!!!! */
	startnetresolv ();
	nokeys = FALSE;
	mvprintw (0, 60, "   KEYS");
	refresh ();
	localkey = oldk;
      }
    }
/*		mvprintw(24,5,"RELOAD=%03d  SENTCLEAR=%d",reload_active,sentclear);
		refresh(); */
    //signal (SIGALRM, dispdata);
   // newtm.it_value.tv_sec = 1;
   // newtm.it_value.tv_usec = 0;
   // setitimer (ITIMER_REAL, &newtm, &oldtm);
  	/*
						 * first screen update in
						 * 1 sec (exact) 
						 */
    if (poschange)
      lcnt = 0;
    if (!routerstats)
      routertime = mnew;
    routerstats++;
    //if (routerstats == ROUTERSTATSMAX)
    //ROUTER STATS.....
    if (topupdate)  // Align stats with 30 sec top flag....
    {
	if (ntop & 1)
        {		
      statsonce++;
      //sec = mnew - routertime;
      sec = TOPUPDATE << 1; // Once a minute....
      use = routeruse - oldrouteruse;
      mintuse = routerto - oldrouterto;
      minfuse = routerfrom - oldrouterfrom;
      routertime = mnew;
      oldrouteruse = routeruse;
      oldrouterto = routerto;
      oldrouterfrom = routerfrom;
      /*
       * CALC STATS for ROUTER  use = # of bytes thru router in "sec"
       * seconds 
       */
      fuse = (double) (use >> 2) / sec;
      dayroute[cur_dayroute++] = (float) fuse;
      if (cur_dayroute >= MAXDAYROUTE)
	cur_dayroute = 0;
      if (!freezedisplay)
	sprintf (&status_lines[0][scrmid - 12], " ROUTER %13.3g kbits/sec ",
		 fuse);
      routerstats = 0;
	}
    }
    xoff = 1;
    tfuse = routerfrom - orouterfrom;
    ttuse = routerto - orouterto;
    ttotal = tfuse + ttuse;
    fuse = (double) (ttotal >> 7);
    if (!lastfreeze && !disableburst && fuse > absmaxburst)
      absmaxburst = fuse;
    if (!lastfreeze && !disableburst && fuse > maxburst)
    {
      maxburst = fuse;
      bursttime = mnew;
      strcpy (burst, ctime (&bursttime));
      burst[strlen (burst) - 1] = 0;
    }
    if (disableburst)
      disableburst = FALSE;

    if (freezedisplay)
    {
      lastfreeze = TRUE;
      UNLOCK(indisp);
      while (semaphore_down(&masterdo)>0);
      uthread_wait(1000);
      continue;
    }
    lastfreeze = FALSE;

//	mvprintw (2, (xright + 5), "LOOP COUNTER=%07d",internal_loop_count);
//	refresh();
    if (rewrite_labels || (printtospeclog && speclog))
    {				/*
				 * if
				 * rewrite_labels 
				 */

      clrportion (1, 1, 4, MCOLS - 1);
      mvprintw (0, 12, "%s", progtitle);
      mvprintw (1, MCOLS - (MCOLS) / 3, "REMOTE NETWORK");
      mvprintw (1, (MCOLS - 22) / 4, "LOCAL NETWORK");
   
      switch (dispopt)
      {
      case DISP_TX_RC:
	mvprintw (2, (xright + 5), "HOST");
	mvprintw (2, (MCOLS - 21), "(PKTS)     X      R");
	mvprintw (2, (xleft + 5), "HOST");
	mvprintw (2, (scrmid - 21), "(PKTS)     X      R");
	if (printtospeclog && speclog)
	  fprintf (fpspeclog, "HOST          (PKTS)     X      R\n\n");
	break;
      case DISP_IPPROTO:
      case DISP_IPPROTON:
	mvprintw (2, (xright + 5), "HOST");
	mvprintw (2, (MCOLS-21),   "     IP     SERVICE");
	mvprintw (2, (xleft + 5), "HOST");
	mvprintw (2, (scrmid-21),   "     IP     SERVICE");
	if (printtospeclog && speclog)
	  fprintf (fpspeclog, "HOST             IP      SERVICE\n\n");
	break;
      case DISP_DEST2:
      case DISP_DEST1:
	mvprintw (2, (xright + 5), "HOST");
	mvprintw (2, (MCOLS-21),   "        TALKING TO ");
	mvprintw (2, (xleft + 5), "HOST");
	mvprintw (2, (scrmid-21),  "        TALKING TO ");
	if (printtospeclog && speclog)
	  fprintf (fpspeclog, "HOST                TALKING TO\n\n");
	break;
      case DISP_ACCT:
	mvprintw (2, (xright + 5), "HOST");
	mvprintw (2, (MCOLS-24),   "(BYTES)   X          R");
	mvprintw (2, (xleft + 5), "HOST");
	mvprintw (2, (scrmid-24),  "(BYTES)   X          R");
	if (printtospeclog && speclog)
	  fprintf (fpspeclog, "HOST      (BYTES)    X          R\n\n");
	break;
      case DISP_INTTXRCP:
	mvprintw (2, (xright + 5), "HOST");
	mvprintw (2, (MCOLS-22),   "LOCAL PKTS  X      R");
	mvprintw (2, (xleft + 5), "HOST");
	mvprintw (2, (scrmid-22),  "LOCAL PKTS  X      R");
	if (printtospeclog && speclog)
	  fprintf (fpspeclog, "HOST      LOCAL PKTS     X      R\n\n");
	break;
      case DISP_EXTTXRCP:
	mvprintw (2, (xright + 5), "HOST");
	mvprintw (2, (MCOLS-21),   "REM. PKTS  X      R");
	mvprintw (2, (xleft + 5), "HOST");
	mvprintw (2, (scrmid-21),  "REM. PKTS  X      R");
	if (printtospeclog && speclog)
	  fprintf (fpspeclog, "HOST       REM. PKTS     X      R\n\n");
	break;
      case DISP_INTTXRC:
	mvprintw (2, (xright + 5), "HOST");
	mvprintw (2, (MCOLS-28),   "LOCAL BYTES  X          R ");
	mvprintw (2, (xleft + 5), "HOST");
	mvprintw (2, (scrmid-28),  "LOCAL BYTES  X          R ");
	if (printtospeclog && speclog)
	  fprintf (fpspeclog, "HOST    LOCAL BYTES  X          R\n\n");
	break;
      case DISP_EXTTXRC:
	mvprintw (2, (xright + 5), "HOST");
	mvprintw (2, (MCOLS-28),   "REM. BYTES   X          R ");
	mvprintw (2, (xleft + 5), "HOST");
	mvprintw (2, (scrmid-28),  "REM. BYTES   X          R ");
	if (printtospeclog && speclog)
	  fprintf (fpspeclog, "HOST    REM. BYTES   X          R\n\n");
	break;
      case DISP_MAC:
	mvprintw (2, (xright + 5), "HOST");
	mvprintw (2, (MCOLS-21),   "   MAC ADDRESS     ");
	mvprintw (2, (xleft + 5), "HOST");
	mvprintw (2, (scrmid-21),  "   MAC ADDRESS     ");
	if (printtospeclog && speclog)
	  fprintf (fpspeclog, "HOST            MAC ADDRESS \n\n");
	break;
      case DISP_BADMAC:
	mvprintw (2, (xright + 5), "HOST");
	mvprintw (2, (MCOLS-22),   "BAD MAC ADDRESS     ");
	mvprintw (2, (xleft + 5), "HOST");
	mvprintw (2, (scrmid-22),  "BAD MAC ADDRESS     ");
	if (printtospeclog && speclog)
	  fprintf (fpspeclog, "HOST            MAC ADDRESS \n\n");
	break;
      case DISP_SERVER:
	mvprintw (2, (xright + 5), "HOST");
	mvprintw (2, (MCOLS-21),   "     HTTP SERVER   ");
	mvprintw (2, (xleft + 5), "HOST");
	mvprintw (2, (scrmid-21),  "     HTTP SERVER   ");
	if (printtospeclog && speclog)
	  fprintf (fpspeclog, "HOST            HTTP SERVER\n\n");
	break;
      case DISP_FTPSERVER:
	mvprintw (2, (xright + 5), "HOST");
	mvprintw (2, (MCOLS-21),   "      FTP SERVER   ");
	mvprintw (2, (xleft + 5), "HOST");
	mvprintw (2, (scrmid-21),  "      FTP SERVER   ");
	if (printtospeclog && speclog)
	  fprintf (fpspeclog, "HOST            FTP SERVER\n\n");
	break;
      case DISP_LASTINTERVAL:
	if (bpsflag)
	{
	mvprintw (2, (xright + 5), "HOST");
	  mvprintw (2, (MCOLS-21), "    INTERVAL kbps  ");
	mvprintw (2, (xleft + 5), "HOST");
	  mvprintw (2, (scrmid-21),"    INTERVAL kbps  ");
	  if (printtospeclog && speclog)
	    fprintf (fpspeclog, "HOST            INTERVAL kbps \n\n");
	}
	else
	{
	mvprintw (2, (xright + 5), "HOST");
	  mvprintw (2, (MCOLS-21), "    INTERVAL BYTES ");
	mvprintw (2, (xleft + 5), "HOST");
	  mvprintw (2, (scrmid-21),"    INTERVAL BYTES ");
	  if (printtospeclog && speclog)
	    fprintf (fpspeclog, "HOST            INTERVAL BYTES\n\n");
	}
	break;
      case DISP_LASTLIVEINT:
	if (bpsflag)
	{
	mvprintw (2, (xright + 5), "HOST");
	  mvprintw (2, (MCOLS-21), "  LIVE INT. kbps   ");
	mvprintw (2, (xleft + 5), "HOST");
	  mvprintw (2, (scrmid-21),"  LIVE INT. kbps   ");
	  if (printtospeclog && speclog)
	    fprintf (fpspeclog, "HOST            LIVE INT. kbps \n\n");
	}
	else
	{
	mvprintw (2, (xright + 5), "HOST");
	  mvprintw (2, (MCOLS-21),"   LIVE INT. BYTES  ");
	mvprintw (2, (xleft + 5), "HOST");
	  mvprintw (2, (scrmid-21),"  LIVE INT. BYTES  ");
	  if (printtospeclog && speclog)
	    fprintf (fpspeclog, "HOST            LIVE INT. BYTES\n\n");
	}
	break;
      case DISP_LASTGET:
	mvprintw (2, (xright + 5), "HOST");
	mvprintw (2, (MCOLS-21),  "       LAST GET     ");
	mvprintw (2, (xleft + 5), "HOST");
	mvprintw (2, (scrmid-21), "       LAST GET     ");
	if (printtospeclog && speclog)
	  fprintf (fpspeclog, "HOST            LAST GET\n\n");
	break;


      }
   
      refresh ();
      rewrite_labels = 0;
    }

    switch (cur_status_line)
    {
    case 0:
      sprintf (&status_lines[0][MCOLS - 15], " Eth:%8ld ", ethcnt);
      status_lines[0][strlen (status_lines[0])] = '~';
      break;
    case 1:
      break;
    case 2:
      break;
    case 3:
      break;
    case 4:
      sprintf (&status_lines[4][3],
	       " NETRESOLV Stats  Sent=%5d   Received=%5d   NoName=%5d",
	       sendresolv, recresolv, timeresolv);
      break;
    case 5:
      sprintf (&status_lines[5][3],
	       " NUMBER of HOSTS --> Local Hosts: %5d   Remote Hosts: %5d ",
	       localcount, remotecount);
      break;
    case 6:
      if (reload_active)
      {
	sprintf (&status_lines[6][3],
		 " Reloading ACTIVE at %02d:%02d  INTERVAL %3d minutes   Reload Count = %5d %d ",
		 rel_hours, rel_mins, reload_timer_sec / 60, reload_active,
		 sentclear);
      }
      else if (wakeup_state)
      {
	sprintf (&status_lines[6][3],
		 " Reloading WAKEUP at %02d:%02d  INTERVAL %3d minutes   Reload Count = %5d ",
		 rel_hours, rel_mins, reload_timer_sec / 60, reload_active);
      }
      else
	sprintf (&status_lines[6][3], " < NO RELOADING INITIALIZED >");
      break;
    case 7:
      sprintf (&status_lines[7][3],
	       " Stats Base File (Reload & L Command): %s ", statsfile);
      break;
    case 8:
      sprintf (&status_lines[8][3],
	       " NETWATCH Configuration File: %s ", specconfigfile);
      break;
    case 9:
      sprintf (&status_lines[9][3], " Log file (" "P" " Command): %s ", mylog);
      break;
    case 10:
      sprintf (&status_lines[10][3],
	       " Watch %8s   with NETMASK=%u.%u.%u.%u  HOST=%u.%u.%u.%u  ",
	       ethdevname, netmask[0], netmask[1], netmask[2], netmask[3],
	       localaddr[0], localaddr[1], localaddr[2], localaddr[3]);
      break;
    case 11:
      break;
    case 12:
      if (isbridge)
	sprintf (&status_lines[12][3], "REPEATS=%d", dupcount);
      else
	sprintf (&status_lines[12][3], "USING FIFO=%d   FIFOVAL=%d",
		 using_fifo, fifoval);

      break;
    default:
      cur_status_line = 0;
    }
   
    dostatus (cur_status_line);
    if (localkey)
    {
      attron (col2);
      mvprintw (1, 1, "KEY");
      attron (col4);
      mvprintw (1, curskeyval, "   ");
    }
    else
    {
      attron (col4);
      mvprintw (1, 1, "   ");
      attron (col2);
      mvprintw (1, curskeyval, "KEY");
      attron (col4);
    }
    ptm = localtime (&mnew);
    strftime (tmbuf, 30, "%c", ptm);
    mvprintw (0, curslines - 25, "%s", tmbuf);
    mvprintw (0, curslines, "LINE %3d ", rydisp);
    mvprintw (0, 2, "LINE %3d ", lydisp);
    if (simchange)
    {
      move (0, scrmid - 3);
      sprintf (simstr, simfmt,
	       simarr[0],
	       simarr[1],
	       simarr[2],
	       simarr[3], ACS_BLOCK, simarr[4], simarr[5], simarr[6]);
      myaddstr (simstr, 7);
      simchange = FALSE;

    }
    if (watch)
    {
      if (disprouterstats)
      {

	if (localkey)
	  xoff = scrmid + 1;
	width = MCOLS - scrmid - 7;
	if (routersummary)
	{
	  mvprintw (6, xoff + 2, "( %2d min. summary   Offset: %4d)", rdelta,
		    router_offset);
	  if (laststatsonce != statsonce || (localkey && refreshrem)
	      || (!localkey && refreshloc))
	  {
	    mvprintw (5, xoff + 10, "ROUTER SUMMARY CHART");
	    clrportion (7, xoff + 2, MLINES - 4, xoff + width + 2);
	    /* RDELTA defaults to 60 minute summaries */
	    num_rsum = 0;
	    rsum[num_rsum] = 0.0;
	    ii = cur_dayroute - 1 - router_offset;
	    if (ii < 0)
	      ii -= MAXDAYROUTE;
	    for (ic = 0; ii != cur_dayroute; ii--)
	    {
	      if (ii < 0)
		ii = MAXDAYROUTE - 1;
	      if (dayroute[ii] >= 0.0)
	      {
		rsum[num_rsum] += dayroute[ii];
		ic++;
		if (ic >= rdelta)
		{
		  ic = 0;
		  num_rsum++;
		  rsum[num_rsum] = 0.0;
		}
	      }
	      else
		break;

	    }
	    mvprintw (7, xoff + 12, "TOP: %4d kbps", (int) absmaxburst);
	    attron (col2);
	    ybase = MLINES - 4;
	    maxlines = MLINES - 11;
	    for (ii = 0; ii < num_rsum && ii < width; ii++)
	    {
	      yh = (int) (rsum[ii] / rdelta / absmaxburst * maxlines) + 1;
	      if ((ii % 10) < 5)
		attron (col2);
	      else
		attron (col3);
	      nspot = xoff + width - ii + 1;
	      mvvline (ybase - yh, nspot, ACS_VLINE, yh);
	      mvprintw (ybase, nspot, "%d", ii % 10);
	    }
	    attron (col4);
	    step5 = (int) (absmaxburst / maxlines * 4.0 + 0.5);
	    for (ii = 5, ic = step5; ii < maxlines; ii += 5, ic += step5)
	    {
	      mvprintw (ybase - ii, xoff + 2, "%d", ic);
	    }
	  }
	}
	else
	{
	  mvprintw (6, xoff + 10, "ROUTER STATS BYTES");
	  mvprintw (8, xoff + 3, "Over 1 second period:");
	  mvprintw (13, xoff + 3, "Over 1 minute period:");
	  mvprintw (9, xoff + 3, " Incoming: %8lu", tfuse);
	  mvprintw (10, xoff + 3, " Outgoing: %8lu", ttuse);
	  mvprintw (11, xoff + 3, " Total:    %8lu", ttotal);
	  mvprintw (11, xoff + 22, "(%5.1f kbps)", fuse);

	  if (statsonce)
	  {
	    mvprintw (14, xoff + 3, " Incoming: %8lu", minfuse);
	    mvprintw (15, xoff + 3, " Outgoing: %8lu", mintuse);
	    mvprintw (16, xoff + 3, " Total:    %8lu", use);
	  }
	  else
	    mvprintw (15, xoff + 3, " N/A");
	  if (maxburst > 1.0)
	  {
	    mvprintw (18, xoff + 3, " MAX. BURST:    %5.1f kbps", maxburst);
	    mvprintw (19, xoff + 3, " at %s", burst);
	  }
	}
      }
      else if (selecthost)
      {
	if (magnafull)
	{
	  xoff = 1;
	  clrportion (6, xoff + 3, MLINES - 4, MCOLS - 3);
	}
	else
	{
	  if (localkey)
	  {
	    xoff = scrmid + 1;
	    clrportion (6, xoff + 3, MLINES - 4, MCOLS - 3);
	  }
	  else
	  {
	    xoff = 1;
	    clrportion (6, xoff + 3, MLINES - 4, scrmid - 3);
	  }
	}
	mvprintw (6, xoff + 4, "TRACE %s", magnamatch);
	mvprintw (7, xoff + 10, "Offset = %3d", magnaoffs);
	if (magnafull)
	{
	  if (magnacomhost == NULL)
	  {
	    pc = (unsigned char *) magnaspot->othaddr;
	    mvprintw (7, MCOLS - 50, "Communicating with %u.%u.%u.%u",
		      pc[0], pc[1], pc[2], pc[3]);

	  }
	  else
	  {
	    LOCK(resolvechange);
	    mvprintw (7, MCOLS - 50, "Communicating with %s",
		      magnacomhost->name);
	    UNLOCK(resolvechange);
	    /*        mvprintw (6, MCOLS - 40, "SERVICES LOC %s  REM %s",
	       servicenm (magnaspot->servicename, magnaspot->port), */
	    /* servicenm (magnacomhost->servicename, magnacomhost->port)); */

	  }
	  mvprintw (6, MCOLS - 40, "SERVICES LOC %s  REM %s",
		    servicenm (magnaspot->servicename, magnaspot->port),
		    servicenm (magnaspot->oth_sn, magnaspot->oth_port));


	}
	dispsize = MLINES - 13;
	if (dispsize > 0)
	{
	  stmag = magnaphys - dispsize;
	  if (stmag < 0)
	    stmag += MAGNAMAX;
	  for (ii = 0, ij = stmag; ii < dispsize; ii++)
	  {
	    displaybuf (&magna[ij], 9 + ii, xoff + 2, ij);
	    ij++;
	    if (ij == MAGNAMAX)
	      ij = 0;
	  }
	}

      }
    }
    laststatsonce = statsonce;
    orouterfrom = routerfrom;
    orouterto = routerto;
    refreshscr++;
    if (refreshscr > MAXREFRESH)
    {
      poschange = 1;
      refreshscr = 0;
    }
    if (printtospeclog)
    {
      if (localkey)
      {
	refreshloc = TRUE;
	refreshrem = FALSE;
      }
      else
      {
	refreshrem = TRUE;
	refreshloc = FALSE;
      }
    }
    if (poschange)
    {
      refreshloc = refreshrem = TRUE;
    }
    if (bluedie == 1)
    {
      refreshloc = refreshrem = TRUE;
      bluedie += 2;
    }
    if (ignoredomain == 1)
    {
      refreshrem = TRUE;
      ignoredomain += 2;
    }
    if (magnafull)
    {
      localbypass = remotebypass = TRUE;
    }
    if (refreshloc && !localbypass)
      clrportion (4, 1, MLINES - 1, scrmid);
    if (refreshrem && !remotebypass)
      clrportion (4, scrmid + 1, MLINES - 1, MCOLS - 1);
    if (!magnafull && (selectchange || refreshloc || refreshrem))
    {
      cleancol (xright - 1, 4, MLINES - 2);
      if (selectside)
	mvaddch (numselect + 4, xright - 1, ACS_LARROW);
      else
	mvaddch (numselect + 4, xright - 1, ACS_RARROW);
    }
/* Construct the List of HEAVY HITTERS for 2 minute periods */
    if (dotop || topupdate)
    {
      dotop = FALSE;
      LOCK(inllist);
      toplocal = &tldummy;
      toplocal->tflink = toplocal;
      current = lfirst->flink;
      while (current != lfirst)
      {
	if (current->tstamp > colred)
	  addtlist (current, toplocal);
	current->timelastbytes = current->timebytes;
	current->timebytes = 0;
	current = current->flink;
      }
      UNLOCK(inllist);
      LOCK(inrlist);
      topremote = &trdummy;
      topremote->tflink = topremote;
      current = rfirst->flink;
      while (current != rfirst)
      {
	if (current->tstamp > colred)
	  addtlist (current, topremote);
	current->timelastbytes = current->timebytes;
	current->timebytes = 0;
	current = current->flink;
      }
      topupdate = FALSE;
      UNLOCK(inrlist);

    }

    if (!localbypass && !llockout && (localupdate || refreshloc))
    {
      mvaddch (3, 0, ACS_VLINE);
      lcnt = 0;
      LOCK(inllist);
      current = lfirst->flink;
      pwork = lfirst;
      if (topflag)
      {
	current = toplocal->tflink;
	pwork = toplocal;
      }
      if (magnaspot == NULL)
	magnaspot = current;
      while (current != pwork)
      {
	y = lcnt;
	if (movemagnakeyloc)
	{
	  if (*(u_int32_t *) current->addr == htonl (searchaddr))
	  {
	    lydisp = y;
	    movemagnakeyloc = FALSE;
	  }
	  else
	  {
	    lcnt++;
	    if (topflag)
	      current = current->tflink;
	    else
	      current = current->flink;
	    continue;

	  }
	}
	if ((!current->pktcntsend && !current->pktcntrec)
	    || (current->tstamp < colgreen && bluedie))
	{
	  if (topflag)
	    current = current->tflink;
	  else
	    current = current->flink;
	  continue;
	}
	lcnt++;
	yact = y - lydisp;
	if (yact >= 0 && yact < MLINES - 5 || printtospeclog)
	{
	  if (!printtospeclog)
	    yact += 4;
	  if (refreshloc || updaterest || current->update)
	  {
	    LOCK(resolvechange);
	    strncpy (thost, current->name, curshosttrim);
	    UNLOCK(resolvechange);
	    if (selecthost && magnakey
		&& magnakey == *((u_int32_t *) current->addr))
	    {
	      if (plogselect)
	      {
		current->plogactive++;
	        current->plogactive &= 1;
		plogselect = 0;
	      }
	      if (plogdeselect)
	      {
		current->plogactive = FALSE;
		plogdeselect = 0;
	      }
	    }


	    if (selectchange && selecthost && numselect + 4 == yact)
	    {
	      magnakey = *((u_int32_t *) current->addr);
	    LOCK(resolvechange);
	      strncpy (magnamatch, current->name, RESOLVE_MAX);
	    UNLOCK(resolvechange);

	      magnaspot = current;
	      magnacomhost = NULL;
	      magnacomkey = *((u_int32_t *) current->othaddr);
	      magnafirst = TRUE;
	      selectchange = FALSE;
	    }
	    thost[curshosttrim] = 0;
	    testt = current->tstamp;
	    if (testt > colred)
	    {
	      attron (col1);
	      if (current->telnet_in)
		attron (col5);
	      if (current->update == 3)
	      {
		ttp = current->badmac;
		sprintf (ttt,
			 "IP %s with BAD MAC = %02x:%02x:%02x:%02x:%02x:%02x",
			 thost, ttp[0], ttp[1], ttp[2], ttp[3], ttp[4],
			 ttp[5]);
		sprintf (&status_lines[3][3], "%s", ttt);
		if (autostatus)
		  cur_status_line = 3;	
		attron (col5);
	      }
	    }
	    else
	    {
	      current->telnet_in = 0;
	      if (testt > colyel)
		attron (col2);
	      else if (testt > colgreen)
	      {
		if (current->plog > 0)
		  close (current->plog);
		attron (col3);
	      }
	      else
		attron (col4);
	    }
	    if (current->plogactive)
	      attron (col6);
	    switch (dispopt)
	    {
	    case DISP_TX_RC:
	      sprintf (ttt, "%*s %6ld %6ld", -curshosttrim - 1, thost,
		       current->pktcntsend, current->pktcntrec);
	      break;
	    case DISP_INTTXRCP:
	      sprintf (ttt, "%*s %6ld %6ld", -curshosttrim - 1, thost,
		       current->intpktcntsend, current->intpktcntrec);
	      break;
	    case DISP_EXTTXRCP:
	      sprintf (ttt, "%*s %6ld %6ld", -curshosttrim - 1, thost,
		       current->extpktcntsend, current->extpktcntrec);
	      break;
	    case DISP_IPPROTO:
	      thost[cursproto] = 0;

	      sprintf (ttt, "%*s %8s %10s   ", -cursproto, thost,
		       current->ip_pr, servicenm (current->servicename,
						  current->port));
	      break;
	    case DISP_IPPROTON:
	      thost[cursdest] = 0;

	      sprintf (ttt, "%*s %8s %5d   ", -cursdest, thost,
		       current->ip_pr, current->port);
	      break;
	    case DISP_DEST1:
	      pc = (unsigned char *) current->othaddr;
	      thost[cursdest] = 0;
	      sprintf (ttt, "%*s %u.%u.%u.%u        ", -cursdest, thost,
		       pc[0], pc[1], pc[2], pc[3]);
	      break;
	    case DISP_DEST2:
	      pc = (unsigned char *) current->othaddr;
	      makeaddr (current->addr, thost);
	      thost[cursdest] = 0;
	      sprintf (ttt, "%*s %u.%u.%u.%u        ", -cursdest, thost,
		       pc[0], pc[1], pc[2], pc[3]);
	      break;
	    case DISP_ACCT:
	      thost[cursproto] = 0;
	      sprintf (ttt, "%*s %10lu %10lu", -cursproto, thost,
		       current->sendbytes, current->recbytes);
	      break;
	    case DISP_EXTTXRC:
	      thost[cursproto] = 0;
	      sprintf (ttt, "%*s %10lu %10lu", -cursproto, thost,
		       current->extsendbytes, current->extrecbytes);
	      break;
	    case DISP_INTTXRC:
	      thost[cursproto] = 0;
	      sprintf (ttt, "%*s %10lu %10lu", -cursproto, thost,
		       current->intsendbytes, current->intrecbytes);
	      break;
	    case DISP_MAC:
	      thost[cursproto] = 0;
	      ttp = current->mac;
	      sprintf (ttt, "%*s %02x:%02x:%02x:%02x:%02x:%02x", -cursproto,
		       thost, ttp[0], ttp[1], ttp[2], ttp[3], ttp[4], ttp[5]);
	      break;
	    case DISP_BADMAC:
	      thost[cursproto] = 0;
	      ttp = current->badmac;
	      sprintf (ttt, "%*s %02x:%02x:%02x:%02x:%02x:%02x", -cursproto,
		       thost, ttp[0], ttp[1], ttp[2], ttp[3], ttp[4], ttp[5]);
	      break;
	    case DISP_SERVER:
	      thost[cursproto] = 0;
	      sprintf (ttt, "%*s %-25s", -cursproto, thost, current->server);
	      break;
	    case DISP_FTPSERVER:
	      thost[cursproto] = 0;
	      sprintf (ttt, "%*s %-25s", -cursproto, thost,
		       current->ftpserver);
	      break;
	    case DISP_LASTINTERVAL:
	      thost[cursdest] = 0;
	      if (bpsflag)
	      {
		sprintf (ttt, "%*s %15.2lf", -cursdest, thost,
			 current->timelastbytes / (double) TOPUPDATE * 0.008);
	      }
	      else
	      {
		sprintf (ttt, "%*s %15lu", -cursdest, thost,
			 current->timelastbytes);
	      }
	      break;
	    case DISP_LASTLIVEINT:
	      thost[cursdest] = 0;
	      if (bpsflag)
	      {
		sprintf (ttt, "%*s %15.2lf", -cursdest, thost,
			 current->timebytes / (double) rs * 0.008);
	      }
	      else
	      {
		sprintf (ttt, "%*s %15lu", -cursdest, thost,
			 current->timebytes);
	      }
	      break;
	    case DISP_LASTGET:
	      thost[cursproto] = 0;
	      sprintf (ttt, "%*s %-25s", -cursproto, thost, current->lastget);
	      break;
	    }
	    ttt[scrmid - 2] = 0;
	    if (printtospeclog && speclog)
	      fprintf (fpspeclog, "%s\n", ttt);
	    else
	      //!!mvprintw (yact, xleft, "%s",ttt);
	      mvprintw (yact, xleft, ttt);
	    attron (col4);
	    if (current->update)
	    {
	      if (current->update == 1)
		updaterest = 1;
	      current->update = 0;
	    }
	  }
	}
	else
	{
	  if (yact < 0)
	    mvaddch (3, 0, ACS_UARROW);
	  else
	    mvaddch (3, 0, ACS_DARROW);
	}
	if (topflag)
	  current = current->tflink;
	else
	  current = current->flink;
      }

      localupdate = 0;
      UNLOCK(inllist);
    }
 
 
    if (!remotebypass && !rlockout && (remoteupdate || refreshrem))
    {
      mvaddch (3, MCOLS - 1, ACS_VLINE);
      rcnt = 0;
      LOCK(inrlist);
      current = rfirst->flink;
      pwork = rfirst;
      if (topflag)
      {
	current = topremote->tflink;
	pwork = topremote;
      }
      if (magnaspot == NULL)
	magnaspot = current;
      while (current != pwork)
      {
	y = rcnt;
	tmpservice = current->servicename;
	if (!tmpservice)
	  tmpservice = emptystr;
	if (movemagnakeyrem)
	{
	  if (*(u_int32_t *) current->addr == htonl (searchaddr))
	  {
	    rydisp = y;
	    movemagnakeyrem = FALSE;
	  }
	  else
	  {
	    rcnt++;
	    if (topflag)
	      current = current->tflink;
	    else
	      current = current->flink;
	    continue;

	  }
	}
	if ((!current->pktcntsend && !current->pktcntrec) ||
	    (current->tstamp < colgreen && bluedie) ||
	    (ignoredomain && !strcmp (tmpservice, "domain")))
	{
	  if (topflag)
	    current = current->tflink;
	  else
	    current = current->flink;
	  continue;
	}
	rcnt++;
	yact = y - rydisp;
	if ((yact >= 0 && yact < MLINES - 5) || printtospeclog)
	{
	  if (!printtospeclog)
	    yact += 4;
	  if (refreshrem || updaterest || current->update)
	  {
	    LOCK(resolvechange);
	    strncpy (thost, current->name, curshosttrim);
	    UNLOCK(resolvechange);
	    if (selecthost && magnakey
		&& magnakey == *((u_int32_t *) current->addr))
	    {
	      if (plogselect)
	      {
		current->plogactive = TRUE;
		plogselect = 0;
	      }
	      if (plogdeselect)
	      {
		current->plogactive = FALSE;
		plogdeselect = 0;
	      }
	    }

	    if (selectchange && selecthost && numselect + 4 == yact)
	    {
	      magnakey = *((u_int32_t *) current->addr);
	    LOCK(resolvechange);
	      strncpy (magnamatch, current->name, RESOLVE_MAX);
	    UNLOCK(resolvechange);
	      magnaspot = current;
	      magnacomhost = NULL;
	      magnacomkey = *((u_int32_t *) current->othaddr);
	      magnafirst = TRUE;
	      selectchange = FALSE;
	    }
	    thost[curshosttrim] = 0;
	    testt = current->tstamp;
	    if (testt > colred)
	    {
	      attron (col1);
	      if (current->telnet_in)
		attron (col5);
	      if (current->update == 3)
	      {
		ttp = current->badmac;
		sprintf (ttt,
			 "IP %s with BAD MAC = %02x:%02x:%02x:%02x:%02x:%02x",
			 thost, ttp[0], ttp[1], ttp[2], ttp[3], ttp[4],
			 ttp[5]);
		if (printtospeclog && speclog)
		  fprintf (fpspeclog, "%s\n", ttt);
		else
		  mvprintw (MLINES - 1, 2, "%s",ttt);
		attron (col5);
	      }
	    }
	    else
	    {
	      current->telnet_in = 0;
	      if (testt > colyel)
		attron (col2);
	      else if (testt > colgreen)
	      {
		if (current->plog > 0)
		  close (current->plog);
		attron (col3);
	      }
	      else
		attron (col4);
	    }
	    if (current->plogactive)
	      attron (col6);
	    switch (dispopt)
	    {
	    case DISP_TX_RC:
	    case DISP_INTTXRCP:
	    case DISP_EXTTXRCP:
	      sprintf (ttt, "%*s %6ld %6ld", -curshosttrim - 1, thost,
		       current->pktcntsend, current->pktcntrec);
	      break;
	    case DISP_IPPROTO:
	      thost[cursproto] = 0;
	      sprintf (ttt, "%*s %8s %10s   ", -cursproto, thost,
		       current->ip_pr, servicenm (current->servicename,
						  current->port));
	      break;
	    case DISP_IPPROTON:
	      thost[cursdest] = 0;

	      sprintf (ttt, "%*s %8s %5d   ", -cursdest, thost,
		       current->ip_pr, current->port);
	      break;
	    case DISP_DEST1:
	      pc = (unsigned char *) current->othaddr;
	      thost[cursdest] = 0;
	      sprintf (ttt, "%*s %u.%u.%u.%u        ", -cursdest, thost,
		       pc[0], pc[1], pc[2], pc[3]);
	      break;
	    case DISP_DEST2:
	      pc = (unsigned char *) current->othaddr;
	      makeaddr (current->addr, thost);
	      thost[cursdest] = 0;
	      sprintf (ttt, "%*s %u.%u.%u.%u        ", -cursdest, thost,
		       pc[0], pc[1], pc[2], pc[3]);
	      break;
	    case DISP_ACCT:
	    case DISP_EXTTXRC:
	    case DISP_INTTXRC:
	      thost[cursproto] = 0;
	      sprintf (ttt, "%*s %10lu %10lu", -cursproto, thost,
		       current->sendbytes, current->recbytes);
	      break;
	    case DISP_MAC:
	      thost[cursproto] = 0;
	      ttp = current->mac;
	      sprintf (ttt, "%*s %02x:%02x:%02x:%02x:%02x:%02x", -cursproto,
		       thost, ttp[0], ttp[1], ttp[2], ttp[3], ttp[4], ttp[5]);
	      break;
	    case DISP_BADMAC:
	      thost[cursproto] = 0;
	      ttp = current->badmac;
	      sprintf (ttt, "%*s %02x:%02x:%02x:%02x:%02x:%02x", -cursproto,
		       thost, ttp[0], ttp[1], ttp[2], ttp[3], ttp[4], ttp[5]);
	      break;
	    case DISP_SERVER:
	      thost[cursproto] = 0;
	      sprintf (ttt, "%*s %-25s", -cursproto, thost, current->server);
	      break;
	    case DISP_FTPSERVER:
	      thost[cursproto] = 0;
	      sprintf (ttt, "%*s %-25s", -cursproto, thost,
		       current->ftpserver);
	      break;
	    case DISP_LASTINTERVAL:
	      thost[cursdest] = 0;
	      if (bpsflag)
	      {
		sprintf (ttt, "%*s %15.2lf", -cursdest, thost,
			 current->timelastbytes / (double) TOPUPDATE * 0.008);
	      }
	      else
	      {
		sprintf (ttt, "%*s %15lu", -cursdest, thost,
			 current->timelastbytes);
	      }
	      break;
	    case DISP_LASTLIVEINT:
	      thost[cursdest] = 0;
	      if (bpsflag && rs)
	      {
		sprintf (ttt, "%*s %15.2lf", -cursdest, thost,
			 current->timebytes / (double) rs * 0.008);
	      }
	      else
	      {
		sprintf (ttt, "%*s %15lu", -cursdest, thost,
			 current->timebytes);
	      }
	      break;
	    case DISP_LASTGET:
	      thost[cursproto] = 0;
	      sprintf (ttt, "%*s %-25s", -cursproto, thost, current->lastget);
	      break;
	    }
	    ttt[scrmid - 2] = 0;
	    if (printtospeclog)
	      fprintf (fpspeclog, "%s\n", ttt);
	    else
	      //!!mvprintw (yact, xright,"%s", ttt);
	      mvprintw (yact, xright, ttt);
	    attron (col4);
	    if (current->update)
	    {
	      if (current->update == 1)
		updaterest = 1;
	      current->update = 0;
	    }
	  }
	}
	else
	{
	  if (yact < 0)
	    mvaddch (3, MCOLS - 1, ACS_UARROW);
	  else
	    mvaddch (3, MCOLS - 1, ACS_DARROW);
	}
	if (topflag)
	  current = current->tflink;
	else
	  current = current->flink;
      }
      remoteupdate = 0;
      UNLOCK(inrlist);
    }
 
    poschange = 0;
    refresh ();
    if (printtospeclog && speclog && !llockout && !rlockout)
    {
      printtospeclog = FALSE;
      fclose (fpspeclog);
      speclogext++;
      sprintf (ttt, "%s.%03d", speclogfile, speclogext);
      fpspeclog = fopen (ttt, "w");
    }
    UNLOCK(indisp);
    while (semaphore_down(&masterdo)>0);
//  semaphore_down (&masterdo);
    uthread_wait(1000);
    internal_loop_count++;
  }
  while (!destroy_program);
  clrscr();
}
