%define name    netwatch 
%define version 1.3.0
%define release 1
 
Summary: Ethernet/PPP IP Packet Monitor
Name: netwatch
Version: %{version} 
Release: %{release}
License: GPL
Group: System/Network
Source: %{name}-%{version}-%{release}.tgz
Group:          Monitoring 
URL: http://www.slctech.org/~mackay/netwatch.html
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-buildroot 
Vendor: Gordon MacKay Consulting
Packager: Gordon MacKay <mackay@uno.slctech.org>

%description
The software enables real-time viewing of network activity.
Network usage is tracked on a per host basis. Packet
and byte counts are available for all host communication.
Router statistics and summary charts are available.

%prep
%setup 

%build
%configure
%make

%install
%makeinstall

%files
#%defattr(0755,root,root) 
%doc README
%doc README.performance
%doc TODO
%doc COPYING
%doc CHANGES
%doc BUGS
%doc netwatch-%{version}.lsm
%{_bindir}/%{name} 
%{_bindir}/netresolv 
%{_mandir}/man1/%{name}.1.lzma

