#include <sys/types.h>
#include <sys/socket.h>
#include <sys/file.h>
#include <sys/time.h>

#include <netinet/in.h>

#include <netdb.h>
#include <stdio.h>

#define BLKSIZE 1024

struct	sockaddr_in sin;
struct	servent *sp;
struct	hostent *host;

unsigned long starts, startms, stops, stopms, expms;
struct timeval ti; 
struct timezone tiz;

char 	greet[BLKSIZE] = "Hi!";
int 	nblocks;
int	f;

int
main(argc, argv)
     int argc;
     char **argv;
{
	register int i;

	if (argc!=3)
	{
		fprintf(stderr, "usage: tcpblast destination nblkocks\n");
		fprintf(stderr, "blocksize: %d bytes\n", BLKSIZE);
		exit(1);
	}

	nblocks = atoi(argv[2]);
	if (nblocks<=1 || nblocks>=10000)
	{
		fprintf(stderr, "tcpblast: 1 < nblocks <= 10000 \n");
		exit(1);
	}

	bzero((char *)&sin, sizeof (sin));
	sin.sin_family = AF_INET;
	f = socket(AF_INET, SOCK_STREAM, 0);
	if (f < 0) {
		perror("tcpblast: socket");
		exit(3);
	}
	{
	  int bufsize, size;
	  getsockopt(f, SOL_SOCKET, SO_SNDBUF, &bufsize, &size);
	  printf("SO_SNDBUF = %d\n", bufsize);
#if 0
	  bufsize = 16*1024;
	  setsockopt(f, SOL_SOCKET, SO_SNDBUF, &bufsize, sizeof(bufsize));
	  getsockopt(f, SOL_SOCKET, SO_SNDBUF, &bufsize, &size);
	  printf("SO_SNDBUF = %d\n", bufsize);
#endif
	}
	if (bind(f, &sin, sizeof (sin)) < 0) {
		perror("tcpblast: bind");
		exit(1);
	}

	host = gethostbyname(argv[1]);
	if (host) {
		sin.sin_family = host->h_addrtype;
		bcopy(host->h_addr, &sin.sin_addr, host->h_length);
	} else {
		sin.sin_family = AF_INET;
		sin.sin_addr.s_addr = inet_addr(argv[1]);
		if (sin.sin_addr.s_addr == -1) {
			fprintf(stderr, "tcpblast: %s unknown host\n", argv[1]);
			exit(1);
		}
	}
	sin.sin_port = htons(9);

	if (connect(f, &sin, sizeof(sin)) <0)
	{
		perror("tcpblast connect:");
		exit(1);
	}

	if (gettimeofday(&ti, &tiz) < 0)
	{
		perror("tcpblast time:");
		exit(1);
	}
	starts  = ti.tv_sec;
	startms = ti.tv_usec / 1000L;


	for (i=0; i<nblocks; i++)
	{
		if (write(f, greet, BLKSIZE) != BLKSIZE)
			perror("tcpblast send:");
		write(1, ".", 1);
	}

	if (gettimeofday(&ti, &tiz) < 0)
	{
		perror("tcpblast time:");
		exit(1);
	}
	stops  = ti.tv_sec;
	stopms = ti.tv_usec / 1000L;

	expms = (stops-starts)*1000 + (stopms-startms);
	printf("\n%d KB in %ld msec", nblocks, expms);
	printf("  =  %.1f kbit/s", ((double) (nblocks*BLKSIZE))/expms*8.0);
	printf("   %d KB/sec\n", (nblocks*BLKSIZE)/expms);
}
