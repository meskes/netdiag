Summary: Primitive banwidth measurement/clogging tool
Name: tcpblast
Version: 20011111
Release: 1
Group: Applications/System
Source: ftp://ftp.6bone.pl/pub/blast/tcpblast-20011111.tar.gz
Copyright: GPL
BuildRoot: /var/tmp/%{name}-root

%description
Sends TCP or UDP packets to discard (or specified) port. Can send with specific
limit for a specified time or without limit. Bidirectional with use of echo
service. Much more - see --help.  See README for tools more suitable for
measuring (not clogging) links capacity.

%prep
%setup -q -n tcpblast-20011111
#%patch0 -p1

%build
# binaries do not know about the prefix
./configure --prefix=$RPM_BUILD_ROOT/usr
make

%install
rm -rf $RPM_BUILD_ROOT
make install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc docs/README
/usr/sbin/*
/usr/man/man8/*

%changelog
* Tue Oct 24 2000 Rafal Maszkowski <rzm@icm.edu.pl>
- updated and included man page again

* Mon Sep  4 2000 Rafal Maszkowski <rzm@icm.edu.pl>
- excluded completely outdated man page

* Tue Nov 16 1999 Rafal Maszkowski <rzm@icm.edu.pl>
- RPM made with 19991116beta
