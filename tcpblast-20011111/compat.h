#include <config.h>

/* for Solaris 2.6 */
#ifndef INADDR_NONE
#  ifndef INADDR_BROADCAST
#    define	INADDR_NONE	-1
#  else
#    define	INADDR_NONE	INADDR_BROADCAST
#  endif
#endif


#ifndef HAVE_STRSEP
char *strsep (char **stringp, const char *delim);
#endif /* HAVE_STRSEP */


#ifndef HAVE_DAEMON
int daemon(int nochdir, int noclose);
#endif /* HAVE_DAEMON */


#ifndef HAVE_TIMERSUB

#define timeradd(a, b, result)                                                \
  do {                                                                        \
    (result)->tv_sec = (a)->tv_sec + (b)->tv_sec;                             \
    (result)->tv_usec = (a)->tv_usec + (b)->tv_usec;                          \
    if ((result)->tv_usec >= 1000000)                                         \
      {                                                                       \
        ++(result)->tv_sec;                                                   \
        (result)->tv_usec -= 1000000;                                         \
      }                                                                       \
  } while (0)
#define timersub(a, b, result)                                                \
  do {                                                                        \
    (result)->tv_sec = (a)->tv_sec - (b)->tv_sec;                             \
    (result)->tv_usec = (a)->tv_usec - (b)->tv_usec;                          \
    if ((result)->tv_usec < 0) {                                              \
      --(result)->tv_sec;                                                     \
      (result)->tv_usec += 1000000;                                           \
    }                                                                         \
  } while (0)

#endif /* HAVE_TIMERSUB */


#ifndef HAVE_SOCKLEN_T
typedef unsigned int socklen_t;
#endif /* HAVE_SOCKLEN_T */
