/*  set_null.c is part of Statnet */
/* Statnet is protected under the GNU Public License (GPL2). */
/* Author: Jeroen Baekelandt (jeroenb@igwe.vub.ac.be)       */


#include "stat.h"

void
set_null ( struct StatMemStruct *StatMem )
{

  SM_regis.ethercount = 0;
  SM_regis.etherbytes = 0;
  SM_regis.plipcount = 0;
  SM_regis.plipbytes = 0;
  SM_regis.slipcount = 0;
  SM_regis.slipbytes = 0;
  SM_regis.pppcount = 0;
  SM_regis.pppbytes = 0;
  SM_regis.loopcount = 0;
  SM_regis.loopbytes = 0;
  SM_regis.othercount = 0;
  SM_regis.otherbytes = 0;
  SM_regis.aarp = 0;
  SM_regis.rtmprd = 0;
  SM_regis.nbp = 0;
  SM_regis.atp = 0;
  SM_regis.aep = 0;
  SM_regis.rtmpreq = 0;
  SM_regis.zip = 0;
  SM_regis.adsp = 0;
  SM_regis.new_ethernet_count = 0;
  SM_regis.unknown_type = 0;
  SM_regis.unknown_frame_type = 0;
  SM_regis.unknown_sap = 0;

  if (options.ip_option)
    memset (&StatMem->ip_protocol_count, 0, sizeof (int) * SN_MAX_IP_PORT + 1);
  if (options.prot_option)
    memset (StatMem->prot.t_count, 0, sizeof (unsigned int) * StatMem->prot.t_max + 1);
  if (options.sap_option)
    memset (&StatMem->sap_count, 0, sizeof (int) * SN_MAX_SAP + 1);

}
