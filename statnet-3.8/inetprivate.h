/*  inetprivate.h is part of Statnet */
/* Statnet is protected under the GNU Public License (GPL2).      */
/* Author: Scot E. Wilcoxon <sewilco@fieldday.mn.org>             */
/**/
/* Define some relatively private symbols. */

#define _PATH_STATNET_SERVICES	"/etc/opt/statnet/services"
#define _PATH_LOCAL_SERVICES	"services"

#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>

