/*  win_show_stat.c is part of Statnet */
/* Statnet is protected under the GNU Public License (GPL2). */
/* Author: Jeroen Baekelandt (jeroenb@igwe.vub.ac.be)       */
/* 05FEB98: Scot E. Wilcoxon (sewilco@fieldday.mn.org)      */

#include <values.h>
#include "stat.h"
#include "curs.h"

void win_show_stat( WINDOW *win, int X, int Y, int noframes, struct Tally *Now_ts, struct Tally *Prev_ts, struct Tally *Delta_ts, short rewrite_labels, short *update_labels, void *show_labels() )
{

  if (win != NULL)
    {                           /* if window to be shown */
      int	port_int, port_now, now_value;
      int	last_value;
      unsigned int	search_int;

      if (rewrite_labels || *update_labels)
        {
          show_labels ( Now_ts );
          *update_labels = 0;
        }

      if( Now_ts->c_show_max > 0 &&
	      Now_ts->c_show_list != (int *)0 &&
	      Delta_ts->count != (unsigned int *)0 )
      {  /* if an array of counters to display */
        last_value = 0;
        for (port_int = 0; port_int < Now_ts->c_show_max; port_int++)
	{	/* do any needed sorting */
	  if ((port_now = Now_ts->c_show_list[port_int]) >= 0)
	    {			/* if a counter to display is defined */
	      now_value = Delta_ts->count[port_now];

	      if (now_value > last_value)
		{		/* if should move this higher on the list */
		  if (port_int > 0 && (now_value > SN_LIST_SWAP || Now_ts->c_show_list[port_int - 1] < 0 || port_int == Now_ts->c_show_max-1))
		    {		/* if not already at the top of the list, swap */
		      Now_ts->c_show_list[port_int] = Now_ts->c_show_list[port_int - 1];
		      Now_ts->c_show_list[port_int - 1] = port_now;
		      *update_labels = 1;
		    }		/* if not already at the top of the list, swap */
		}		/* if should move this higher on the list */
	      last_value = now_value;
	    }			/* if a counter to display is defined */
	  else
	    last_value = 0;
	}	/* do any needed sorting */
        for (port_int = 0; port_int < Now_ts->c_show_max; port_int++)
	{			/* for all counters to display */
	  if ((port_now = Now_ts->c_show_list[port_int]) >= 0)
	    {			/* if a counter to display is defined */
	      now_value = Delta_ts->count[port_now];
	      mvwprintw (win, Y + port_int, X, "%5d %5.1f%%", now_value, now_value * 100.0 / noframes);
	      last_value = now_value;
	    }			/* if a counter to display is defined */
	  else
	    last_value = 0;
	}			/* for all counters to display */

      if (last_value == 0)
	{			/* if last item on the screen is zero, find something to replace it */

	  for (search_int = 0; search_int <= Now_ts->c_max; search_int++)
	    {			/* for all counters which might be displayed */

	      if (now_value = Delta_ts->count[search_int])
		{		/* if another port with activity was found */
		  for (port_int = 0; port_int < Now_ts->c_show_max; port_int++)
		    {		/* for all TCP protocols to display */
		      if (Now_ts->c_show_list[port_int] == search_int) break;	/* if this has been displayed, don't need to consider it further */
		      if (Now_ts->c_show_list[port_int] < 0)
			{	/* if a TCP protocol to display is not defined for this field */
			  Now_ts->c_show_list[port_int] = search_int;
			  mvwprintw (win, Y + port_int, X, "%5d %5.1f%%", now_value, now_value * 100.0 / noframes);
		          *update_labels = 1;
			  break;
			}	/* if a TCP protocol to display is not defined for this field */
		    }		/* for all TCP protocols to display */
		  if (port_int >= Now_ts->c_show_max)
		    {		/* if no empty TCP protocol slot was found */
		      Now_ts->c_show_list[Now_ts->c_show_max - 1] = search_int;
		      mvwprintw (win, Y+Now_ts->c_show_max-1, X, "%5d %5.1f%%", now_value, now_value * 100.0 / noframes);
		      *update_labels = 1;
		      break;
		    }		/* if no empty TCP protocol slot was found */
		}		/* if another port with activity was found */
	    }			/* for all counters which might be displayed */
	}			/* if last item on the screen is zero, find something to replace it */
      }  /* if an array of counters to display */



      if( Now_ts->t_max > 0 )
      {  /* if an array of types to display */
        for (port_int = 0; port_int < Now_ts->t_max; port_int++)
        {                       /* for all types to display */
          if ((port_now = Now_ts->types[port_int]) > 0)
            {                   /* if a type to display is defined for this field */
              if ( Prev_ts->types[port_int] != Now_ts->types[port_int] )
              {  /* if types have changed */
                      *update_labels = 1;
              }  /* if types have changed */
              now_value = Delta_ts->t_count[port_int];
              mvwprintw (win, port_int + Y, X, "%5d %5.1f%%", now_value, now_value * 100.0 / noframes);
            }                   /* if a type to display is defined for this field */
        }                       /* for all types to display */
      }  /* if an array of types to display */

      if (rewrite_labels || *update_labels)
        {
          show_labels ( Now_ts );
          *update_labels = 0;
        }

    }                           /* if window to be shown */
}  /* win_show_stat() */

