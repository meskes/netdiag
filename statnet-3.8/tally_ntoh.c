/*  tally_ntoh.c is part of Statnet */
/* Statnet is protected under the GNU Public License (GPL2). */
/* Author: Jeroen Baekelandt (jeroenb@igwe.vub.ac.be)        */
/* 13MAR98: Scot E. Wilcoxon (sewilco@fieldday.mn.org)       */

#include "stat.h"
  
/* Copy a pointer which is relative to the start of a block of memory */
#define PtrCpy(A,B,AP,BP) AP = (void *)((A) + (((void *)BP) - ((void *)B)));

void
tally_ntoh ( int bytecode,
	struct StatMemStruct *New,
	struct StatMemStruct *Prev,
	struct Tally *New_t, 
	struct Tally *Prev_t )
{
  unsigned	temp_int, search;

  if( New_t != (struct Tally *)0 &&
      Prev_t != (struct Tally *)0 )
  {  /* if pointers initialized */

    switch( bytecode )
    {  /* switch bytecode */
      default:
      case 0:
      case 1:
	    New_t->type_code = Prev_t->type_code;

	    if( Prev_t->count == (unsigned int *)0 )
		    New_t->count = (unsigned int *)0;
	    else
		    PtrCpy(New,Prev,New_t->count,Prev_t->count);

	    New_t->c_show_max = Prev_t->c_show_max;
	    New_t->c_max = Prev_t->c_max;

	    if( Prev_t->c_show_list == (int *)0 )
		    New_t->c_show_list = (int *)0;
	    else
		    PtrCpy(New,Prev,New_t->c_show_list,Prev_t->c_show_list);

	    if( Prev_t->c_labels == (char *)0 )
		    New_t->c_labels = (char *)0;
	    else
		    PtrCpy(New,Prev,New_t->c_labels,Prev_t->c_labels);

	    if( Prev_t->types == (unsigned int *)0 )
		    New_t->types = (unsigned int *)0;
	    else
		    PtrCpy(New,Prev,New_t->types,Prev_t->types);

	    if( Prev_t->t_count == (unsigned int *)0 )
		    New_t->t_count = (unsigned int *)0;
	    else
		    PtrCpy(New,Prev,New_t->t_count,Prev_t->t_count);

	    if( Prev_t->t_values == (unsigned int *)0 )
		    New_t->t_values = (unsigned int *)0;
	    else
		    PtrCpy(New,Prev,New_t->t_values,Prev_t->t_values);

	    New_t->t_max = Prev_t->t_max;
	    New_t->t_max_labels = Prev_t->t_max_labels;

	    if( Prev_t->t_labels == (char *)0 )
		    New_t->t_labels = (char *)0;
	    else
		    PtrCpy(New,Prev,New_t->t_labels,Prev_t->t_labels);
	    break;
      case 2:
	    New_t->type_code = Prev_t->type_code;

	    if( Prev_t->count == (unsigned int *)0 )
		    New_t->count = (unsigned int *)0;
	    else
		    PtrCpy(New,Prev,New_t->count,ntohl((int)Prev_t->count));

	    New_t->c_show_max = Prev_t->c_show_max;
	    New_t->c_max = Prev_t->c_max;

	    if( Prev_t->c_show_list == (int *)0 )
		    New_t->c_show_list = (int *)0;
	    else
		    PtrCpy(New,Prev,New_t->c_show_list,ntohl((int)Prev_t->c_show_list));

	    if( Prev_t->c_labels == (char *)0 )
		    New_t->c_labels = (char *)0;
	    else
		    PtrCpy(New,Prev,New_t->c_labels,ntohl((int)Prev_t->c_labels));

	    if( Prev_t->types == (unsigned int *)0 )
		    New_t->types = (unsigned int *)0;
	    else
		    PtrCpy(New,Prev,New_t->types,ntohl((int)Prev_t->types));

	    if( Prev_t->t_count == (unsigned int *)0 )
		    New_t->t_count = (unsigned int *)0;
	    else
		    PtrCpy(New,Prev,New_t->t_count,ntohl((int)Prev_t->t_count));

	    if( Prev_t->t_values == (unsigned int *)0 )
		    New_t->t_values = (unsigned int *)0;
	    else
		    PtrCpy(New,Prev,New_t->t_values,ntohl((int)Prev_t->t_values));

	    New_t->t_max = Prev_t->t_max;
	    New_t->t_max_labels = Prev_t->t_max_labels;

	    if( Prev_t->t_labels == (char *)0 )
		    New_t->t_labels = (char *)0;
	    else
		    PtrCpy(New,Prev,New_t->t_labels,ntohl((int)Prev_t->t_labels));
	    break;
      case 3:
	    New_t->type_code = Prev_t->type_code;

	    if( Prev_t->count == (unsigned int *)0 )
		    New_t->count = (unsigned int *)0;
	    else
		    PtrCpy(New,Prev,New_t->count,htonl((int)Prev_t->count));

	    New_t->c_show_max = Prev_t->c_show_max;
	    New_t->c_max = Prev_t->c_max;

	    if( Prev_t->c_show_list == (int *)0 )
		    New_t->c_show_list = (int *)0;
	    else
		    PtrCpy(New,Prev,New_t->c_show_list,htonl((int)Prev_t->c_show_list));

	    if( Prev_t->c_labels == (char *)0 )
		    New_t->c_labels = (char *)0;
	    else
		    PtrCpy(New,Prev,New_t->c_labels,htonl((int)Prev_t->c_labels));

	    if( Prev_t->types == (unsigned int *)0 )
		    New_t->types = (unsigned int *)0;
	    else
		    PtrCpy(New,Prev,New_t->types,htonl((int)Prev_t->types));

	    if( Prev_t->t_count == (unsigned int *)0 )
		    New_t->t_count = (unsigned int *)0;
	    else
		    PtrCpy(New,Prev,New_t->t_count,htonl((int)Prev_t->t_count));

	    if( Prev_t->t_values == (unsigned int *)0 )
		    New_t->t_values = (unsigned int *)0;
	    else
		    PtrCpy(New,Prev,New_t->t_values,htonl((int)Prev_t->t_values));

	    New_t->t_max = Prev_t->t_max;
	    New_t->t_max_labels = Prev_t->t_max_labels;

	    if( Prev_t->t_labels == (char *)0 )
		    New_t->t_labels = (char *)0;
	    else
		    PtrCpy(New,Prev,New_t->t_labels,htonl((int)Prev_t->t_labels));
	    break;
    }  /* switch bytecode */

    if (New_t->type_code != disabled)
    {  /* if type not disabled */
      if( New_t->c_max > 0 &&
          New_t->count != (unsigned int *)0 &&
          Prev_t->count != (unsigned int *)0 )
      {  /* if using array of counters */
	for (temp_int = 0; temp_int <= New_t->c_max; temp_int++ )
	{  /* for all in the array of counters */
	    switch( bytecode )
	    {  /* switch bytecode */
	      default:
	      case 0:
	      case 1:
		New_t->count[temp_int] = Prev_t->count[temp_int];
		break;
	      case 2:
		New_t->count[temp_int] = ntohl(Prev_t->count[temp_int]);
		break;
	      case 3:
		New_t->count[temp_int] = htonl(Prev_t->count[temp_int]);
		break;
	    }  /* switch bytecode */
	}  /* for all in the array of counters */
      }  /* if using array of counters */

      if( New_t->t_max > 0 &&
          New_t->types != (unsigned int *)0 &&
          Prev_t->types != (unsigned int *)0 &&
          New_t->t_count != (unsigned int *)0 &&
          Prev_t->t_count != (unsigned int *)0 &&
          New_t->t_values != (unsigned int *)0 &&
          Prev_t->t_values != (unsigned int *)0 )
      {  /* if using array of types */
	for (temp_int = 0; temp_int <= New_t->t_max; temp_int++ )
	{  /* for all types in the array */
	    switch( bytecode )
	    {  /* switch bytecode */
	      default:
	      case 0:
	      case 1:
		New_t->t_count[temp_int] = Prev_t->t_count[temp_int];
		break;
	      case 2:
		New_t->t_count[temp_int] = ntohl(Prev_t->t_count[temp_int]);
		break;
	      case 3:
		New_t->t_count[temp_int] = htonl(Prev_t->t_count[temp_int]);
		break;
	    }  /* switch bytecode */
	}  /* for all types in the array */
      }  /* if using array of types */

    }  /* if type not disabled */
  }  /* if pointers initialized */
}
