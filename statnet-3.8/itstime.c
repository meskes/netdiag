/*  itstime.c is part of Statnet */
/* Statnet is protected under the GNU Public License (GPL2). */
/* Author: Jeroen Baekelandt (jeroenb@igwe.vub.ac.be)       */
/* 02MAR98: Scot E. Wilcoxon (sewilco@fieldday.mn.org)      */

/* Get statistics when timer expires. */

#include "stat.h"
#include <errno.h>
#include <signal.h>

extern struct StatMemStruct *StatMem;	/* pointer to shared memory segment */

void itstime (int errnum)
{
#if 0
/* removed for statnet 3.* due to awkwardness in reading data. */

        if_getstats( ETH, now_stats );	/* get present interface statistics */
        if( last_stats->rx_errors != now_stats->rx_errors ||
            last_stats->rx_dropped != now_stats->rx_dropped )
        {  /* if errors reported */
	  SM_regis.rx_errors_d = now_stats->rx_errors - last_stats->rx_errors;
	  SM_regis.rx_packets_d = now_stats->rx_packets - last_stats->rx_packets;
	  SM_regis.rx_dropped_d = now_stats->rx_dropped - last_stats->rx_dropped;
	  SM_regis.rx_interval = SN_STATS_SECS;

          /* swap last and now stats buffer pointers */
        }  /* if errors reported */
        else
        {  /* else clear the data */
	  SM_regis.rx_errors_d = 0;
	  SM_regis.rx_packets_d = 0;
	  SM_regis.rx_dropped_d = 0;
	  SM_regis.rx_interval = SN_STATS_SECS;
        }  /* else clear the data */
        temp_stats = last_stats;
        last_stats = now_stats;
        now_stats = temp_stats;
#endif

      if (signal (SIGALRM, itstime) == SIG_ERR)
      {
	SM_regis.errcode = errno;
	SM_regis.errcount++;
	timer_flag = 0;
      }
      else
      {  /* else signal registered */
	alarm (SN_STATS_SECS);
	timer_flag = 1;
      }  /* else signal registered */

  return;
}

