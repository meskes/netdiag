/*  tally_label.c is part of Statnet */
/* Statnet is protected under the GNU Public License (GPL2). */
/* Author: Jeroen Baekelandt (jeroenb@igwe.vub.ac.be)       */
/* 15FEB98: Scot E. Wilcoxon (sewilco@fieldday.mn.org)      */

/* This shows labels for a struct Tally list. */

#include "stat.h"
#include "curs.h"

static WINDOW *win;

tally_label ( struct Tally *Now_ts, WINDOW *win, int width, int height )
{
  int	typelen;

  typelen = 1;

  if ( Now_ts != (struct Tally *)0 &&
       Now_ts->type_code != disabled )
    {  /* if not disabled */
      unsigned int temp_int, now_value;

      switch ( Now_ts->type_code )
      {  /* select type_code */
        case protocol:
		typelen = SN_PORT_TYPE_LEN+1;
		mvwprintw (win, 0, 0, "===== PROTOCOLS =====");
		mvwprintw (win, height - 1, 0, "Other:");
		break;
        case appletalk:
		typelen = SN_PORT_TYPE_LEN+1;
		mvwprintw (win, 0, 0, "===== APPLETALK =====");
		break;
        case ip_prot:
		typelen = SN_PORT_TYPE_LEN+1;
		mvwprintw (win, 0, 0, "==== IP PROTOCOLS ====");
		break;
        case tcp:
		typelen = SN_PORT_TYPE_LEN+1;
		mvwprintw (win, 0, 0, "==== TCP/IP PORTS ====");
		break;
        case udp:
		typelen = SN_PORT_TYPE_LEN+1;
		mvwprintw (win, 0, 0, "==== UDP/IP PORTS ====");
		break;
        case sap:
		typelen = SN_PORT_TYPE_LEN+1;
		mvwprintw (win, 0, 0, "==== 802.2 SAP ====");
		break;
        default:
		break;
      }  /* select type_code */

      if( Now_ts->count != (unsigned int *)0 &&
          Now_ts->c_max > 0 &&
          Now_ts->c_labels != (char *)0 &&
          Now_ts->c_show_max > 0 &&
          Now_ts->c_show_list != (int *)0 )
      {  /* if an array of counters */
        for (temp_int = 0; temp_int < height-1 && temp_int < Now_ts->c_show_max; temp_int++)
        {  /* for all counters */
	  if ((now_value=Now_ts->c_show_list[temp_int]) >= 0 && now_value <= Now_ts->c_max)
	    {			/* if something to show */
#if 0
/* this is just for tracing which value is being shown on display */
	      mvwprintw (win, 1 + temp_int, 0, "%3d%*.*s",
                         now_value,
			 6,
			 6,
			 &Now_ts->c_labels[now_value*typelen]);
#else
	      mvwprintw (win, 1 + temp_int, 0, "%*.*s",
			 9,
			 9,
			 &Now_ts->c_labels[now_value*typelen]);
#endif
	    }			/* if something to show */
	  else
	    {			/* else fill the line with spaces */
	      mvwprintw (win, 1 + temp_int, 0, "%*s", width - 1, " ");
	    }			/* else fill the line with spaces */
	}  /* for all counters */
      }  /* if an array of counters */

      if( Now_ts->types != (unsigned int *)0 &&
          Now_ts->t_max > 0 &&
          Now_ts->t_max_labels > 0 &&
          Now_ts->t_labels != (char *)0 )
      {  /* if an array of types */
        for (temp_int = 0; temp_int < Now_ts->t_max; temp_int++)
	{  /* for all types */
	  if (Now_ts->types[temp_int] > 0)
	    {			/* if something to show */
	      mvwprintw (win, 1 + temp_int, 0, "%*.*s",
			 9,
			 9,
			 &Now_ts->t_labels[Now_ts->types[temp_int]*typelen]);
	    }			/* if something to show */
	  else
	    {			/* else fill the line with spaces */
	      mvwprintw (win, 1 + temp_int, 0, "%*s", width - 1, " ");
	    }			/* else fill the line with spaces */
	}  /* for all types */
      }  /* if an array of types */
    }  /* if not disabled */
}
