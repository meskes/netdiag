/*  curs.h is part of Statnet */
/* Statnet is protected under the GNU Public License (GPL2). */
/* Author: Jeroen Baekelandt (jeroenb@igwe.vub.ac.be)       */

#include <curses.h>

void init_curses (void);
void cleanup_curses (void);
void clrscr (void);
