/*  stat.h is part of Statnet */
/* Statnet is protected under the GNU Public License (GPL2).      */
/* Author: Jeroen Baekelandt (jeroenb@igwe.vub.ac.be)             */
/* Modified by: Scot E. Wilcoxon (sewilco@fieldday.mn.org)        */

#define ETH "eth0"
#define SN_UPDATE_SECS		6	/* Number of seconds between updates. */
					/* Suggest 4 to 6 seconds minimum, as */
					/* two seconds is usually too fast to */
					/* see relationships.  CPU time use is */
					/* mostly due to packet processing if */
					/* update is greater than 3 seconds.  */
#if ( SN_UPDATE_SECS > 25 )
#undef SN_UPDATE_SECS
#define SN_UPDATE_SECS		25	/* Max of 25 seconds due to curses */
#endif

#define SN_STATS_SECS		(60)	/* Number of seconds between stats updates */

#define STAT_IPC_KEY		((0x57a7237)%MAXINT)
#define STAT_MAGIC		((0x57a7237)%MAXINT)
#define STATNETD_IFACE_LEN	256	/* max len of statnetd interface name */
#define STATNETD_NAME_LEN	55	/* max len of statnetd machine name */

#define SN_NUM_PROTOCOLS	9	/* Number of Ethernet protocols to show */
#define SN_MAX_PROTO_DESC       200	/* Number of Ethernet protocols to know */
#define SN_NUM_IP_TYPES		7	/* Number of IP protocols to show */
#define SN_NUM_TCP_PORTS	9	/* Number of TCP ports to show */
#define SN_NUM_UDP_PORTS	9	/* Number of UDP ports to show */
#define SN_NUM_SAP_TYPES	9	/* Number of IP protocols to show */
#define SN_MAX_IP_PORT		256	/* Number of IP protocols to tally */
#define SN_MAX_TCP_PORTS 	1024	/* Number of TCP ports to tally */
#define SN_MAX_UDP_PORTS 	1024	/* Number of UDP ports to tally */
#define SN_MAX_SAP		256	/* Number of 802.2 SAP to tally */
#define SN_PORT_TYPE_LEN 	20	/* Length of type labels */
#define SN_LIST_SWAP		5	/* Number of packets to require movement higher in list */

#define SN_PROT_IEEE802_3	165	/* Pseudo protocol for IEEE 802.3 */
#define SN_PROT_SLIP		162	/* Pseudo protocol for SLIP */
#define SN_PROT_PPP		163	/* Pseudo protocol for PPP */
#define SN_PROT_LOOP		164	/* Pseudo protocol for Loopback */

#include <sys/types.h>
#include <netdb.h>

struct registers
  {
    /* Ethernet interface */
    int ethercount;
    long etherbytes;

    /* PLIP interface */
    int plipcount;
    long plipbytes;

    /* SLIP interface */
    int slipcount;
    long slipbytes;

    /* PPP interface */
    int pppcount;
    long pppbytes;

    /* loopback interface */
    int loopcount;
    long loopbytes;

    int othercount;
    long otherbytes;

    /* Appletalk types */
    int aarp;
    int rtmprd;
    int nbp;
    int atp;
    int aep;
    int rtmpreq;
    int zip;
    int adsp;

    /* IEEE802.2 protocol */
    int new_ethernet_count;

    /* unknown types */
    int unknown_type;
    int unknown_frame_type;	/* store last unknown frame code */
    int unknown_sap;		/* store last unknown sap codes */

    /* Received error count and last error code */
    int errcount;
    int errcode;		/* store last error code */

    /* Interface errors */
    int	rx_errors_d;		/* receive errors */
    int rx_dropped_d;		/* dropped frames */
    int rx_packets_d;		/* dropped frames */
    int rx_interval;		/* number of seconds interval for interface errs */

  };

typedef enum	Tally_Type_enum { disabled, protocol, appletalk, ip_prot, tcp, udp, sap } Tally_Type;

struct Tally {
	Tally_Type	type_code;	/* Data type code, useful for headers */
	/* Counters are used for items which can be directly indexed, */
	/* such as port numbers from 0 to 1024 with c_max of 1024. */
	unsigned int	*count;	/* Array of counters */
	int		c_show_max;	/* Maximum show list index value */
	int		c_max;		/* Maximum counter index value */
	int		*c_show_list;	/* Array of counter display info */
	char		*c_labels;	/* Array of counter labels */
	/* Types are used for items which have a large range but only */
	/* a few values in the range are used, such as a dozen protocol */
	/* values which are scattered in a sixteen-bit range. */
	unsigned int	*types;	/* Array of types */
	unsigned int	*t_count;	/* Array of type counters */
	unsigned int	*t_values;	/* Array of type values */
	int		t_max;		/* Maximum type counter index value */
	int		t_max_labels;	/* Maximum type label index value */
	char		*t_labels;	/* Array of type labels */
};

struct StatMemStruct {
	unsigned int		statmem_label;	/* a magic number for ID and endianess */
	unsigned int		statmem_size;	/* sizeof this structure */
	char			servername[STATNETD_NAME_LEN+1];	/* machine running statnetd */
	struct registers	regis;		/* high-level counters */
	struct Tally		prot;		/* Network Protocols */
	struct Tally		ip_types;	/* IP protocols */
	struct Tally		tcp_port;	/* TCP port count */
	struct Tally		udp_port;	/* UDP port count */
	struct Tally		sap_types;	/* SAP type count */

	/* Note the +1 added to arrays to allow direct reference to constant values   */
	/* instead of having to subtract 1 due to array address starting from zero. */
	unsigned int protocol_count[SN_NUM_PROTOCOLS + 1];	/* Ethernet protocol counters */
	unsigned int prot_types[SN_NUM_PROTOCOLS];		/* Ethernet protocol types to display */
	unsigned int tcp_port_ctr[SN_MAX_TCP_PORTS+1];		/* TCP ports */
	unsigned int udp_port_ctr[SN_MAX_UDP_PORTS+1];		/* UDP ports */
	int protocol_num[SN_MAX_PROTO_DESC + 1];	/* Protocol numbers */
	int ip_protocol_count[SN_MAX_IP_PORT + 1];	/* IP protocol count */
	int sap_count[SN_MAX_SAP + 1];			/* SAP count */
};

#define	SM_regis		StatMem->regis

#ifdef MAIN_LINE
#define EXTERN_DEF
#else
#define EXTERN_DEF	extern
#endif

EXTERN_DEF struct {
    int ip_option:1;		/* Types window */
    int g:1;			/* General window */
    int prot_option:1;		/* Protocol activity */
    int at_option:1;		/* Appletalk activity */
    int tcp_option:1;		/* TCP/IP activity */
    int udp_option:1;		/* UDP activity */
    int sap_option:1;		/* SAP activity */
  } options;	/* option values */

EXTERN_DEF int packet_type;
EXTERN_DEF int frame_protocol;;
EXTERN_DEF int rewrite_labels;
EXTERN_DEF int redraw_screen;
EXTERN_DEF int help_flag;
EXTERN_DEF int temp_int;
EXTERN_DEF int timer_flag;
EXTERN_DEF int stats_countdown;
#if 0
/* removed for statnet 3.* due to awkwardness in reading data. */
EXTERN_DEF struct enet_statistics	*now_stats;	/* Ethernet statistics */
EXTERN_DEF struct enet_statistics	*last_stats;	/* previous statistics */
EXTERN_DEF struct enet_statistics *temp_stats;
EXTERN_DEF struct enet_statistics stat_buf1;
EXTERN_DEF struct enet_statistics stat_buf2;
#endif

EXTERN_DEF int IP_types[SN_NUM_IP_TYPES];	/* IP protocol types to display */
EXTERN_DEF int tcp_ports[SN_NUM_TCP_PORTS]; /* TCP port numbers to display */
EXTERN_DEF int udp_ports[SN_NUM_UDP_PORTS]; /* UDP port numbers to display */
EXTERN_DEF int SAP_types[SN_NUM_SAP_TYPES]; /* SAP protocol types to display */

/* Note the +1 added to char strings for the terminating NULL. */
EXTERN_DEF int protocol_num[SN_MAX_PROTO_DESC + 1];	/* Protocol numbers */
EXTERN_DEF char ip_protocol_types[SN_MAX_IP_PORT + 1][SN_PORT_TYPE_LEN + 1];
EXTERN_DEF char tcp_port_types[SN_MAX_TCP_PORTS + 1][SN_PORT_TYPE_LEN + 1];
EXTERN_DEF char udp_port_types[SN_MAX_UDP_PORTS + 1][SN_PORT_TYPE_LEN + 1];
EXTERN_DEF char sap_port_types[SN_MAX_SAP + 1][SN_PORT_TYPE_LEN + 1];
EXTERN_DEF char protocol_types[SN_MAX_PROTO_DESC + 1][SN_PORT_TYPE_LEN + 1];

void close_all_subwin ();
void itstime (int errnum);
void update_display ();
void set_null ( struct StatMemStruct *StatMem );
void services ();
void stat_delta ( struct StatMemStruct *New, struct StatMemStruct *Prev, struct StatMemStruct *Delta );
void usage (char *arg);
#ifdef WINDOW
void win_show_stat( WINDOW *win, int X, int Y, int noframes, struct Tally *Now_ts, struct Tally *Prev_ts, struct Tally *Delta_ts, short rewrite_labels, short *update_labels, void *show_labels );
#endif

void tally_delta ( struct Tally *New_t, 
	struct Tally *Prev_t, 
	struct Tally *Delta_t );

void tally_init ( struct Tally *ts,
	Tally_Type	type_code,
	unsigned int	*count,	/* Array of counters */
	int		c_show_max,
	int		*c_show_list,
	int		c_max,		/* Maximum counter index value */
	char		*c_labels,	/* Array of counter labels */
	unsigned int	*types,	/* Array of types */
	unsigned int	*t_count,	/* Array of type counters */
	unsigned int	*t_values,	/* Array of type values */
	int		t_max,		/* Maximum type counter index value */
	int		t_max_labels,	/* Maximum type label index value */
	char		*t_labels	/* Array of type labels */
);

void tally_ntoh ( int bytecode,
        struct StatMemStruct *New,
        struct StatMemStruct *Prev,
        struct Tally *New_t, 
        struct Tally *Prev_t );

#ifdef WINDOW
void tally_label ( struct Tally *Now_ts, WINDOW *win, int width, int height );
#endif


/* Now some redefinitions of <netdb.h> stuff.  With the Berkeley notice... */

/*-
 * Copyright (c) 1980, 1983, 1988, 1993
 *     The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)netdb.h	8.1 (Berkeley) 6/2/93
 *      netdb.h,v 1.4 1995/08/14 04:05:04 hjl Exp
 * -
 * Portions Copyright (c) 1993 by Digital Equipment Corporation.
 *
 * Permission to use, copy, modify and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies, and that
 * the name of Digital Equipment Corporation not be used in advertising or
 * publicity pertaining to distribution of the document or software without
 * specific, written prior permission.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND DIGITAL EQUIPMENT CORP. DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS.   IN NO EVENT SHALL DIGITAL EQUIPMENT
 * CORPORATION BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
 * ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 * -
 * --Copyright--
 */

void		sn_endhostent __P((void));
void		sn_endnetent __P((void));
void		sn_endprotoent __P((void));
void		sn_endservent __P((void));
void		sn_endrpcent __P ((void));
struct hostent	*sn_gethostbyaddr __P((__const char *, int, int));
struct hostent	*sn_gethostbyname __P((__const char *));
struct hostent	*sn_gethostent __P((void));
struct netent	*sn_getnetbyaddr __P((long, int)); /* u_long? */
struct netent	*sn_getnetbyname __P((__const char *));
struct netent	*sn_getnetent __P((void));
struct protoent	*sn_getprotobyname __P((__const char *));
struct protoent	*sn_getprotobynumber __P((int));
struct protoent	*sn_getprotoent __P((void));
struct servent	*sn_getservbyname __P((__const char *, __const char *));
struct servent	*sn_getservbyport __P((int, __const char *));
struct servent	*sn_getservent __P((void));
struct rpcent	*sn_getrpcent __P((void));
struct rpcent	*sn_getrpcbyname __P((__const char *));
struct rpcent	*sn_getrpcbynumber __P((int));
void		sn_herror __P((__const char *));
void		sn_sethostent __P((int));
/* void		sn_sethostfile __P((__const char *)); */
void		sn_setnetent __P((int));
void		sn_setprotoent __P((int));
void		sn_setservent __P((int));
void		sn_setrpcent __P((int));
