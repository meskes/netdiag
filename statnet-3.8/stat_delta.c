/*  stat_delta.c is part of Statnet */
/* Statnet is protected under the GNU Public License (GPL2). */
/* Author: Jeroen Baekelandt (jeroenb@igwe.vub.ac.be)        */
/* 08JAN98: Scot E. Wilcoxon (sewilco@fieldday.mn.org)       */


#include "stat.h"
#include <stdio.h>

/* Deal with overflow/restart in simpleminded way */
#define DIFFZERO(a,b)	( ( a > b ) ? ( a - b ) : 0 )

void
stat_delta ( struct StatMemStruct *New, 
	struct StatMemStruct *Prev, 
	struct StatMemStruct *Delta )
{
  unsigned	temp_int, search;

  Delta->prot.types = Delta->prot_types;
  Delta->prot.t_count = Delta->protocol_count;

  /* A few items which are not simple counters */
  Delta->regis.unknown_type = New->regis.unknown_type;
  Delta->regis.unknown_frame_type = New->regis.unknown_frame_type;
  Delta->regis.unknown_sap = New->regis.unknown_sap;
  Delta->regis.errcode = New->regis.errcode;

  Delta->regis.errcount = DIFFZERO( New->regis.errcount, Prev->regis.errcount );
  Delta->regis.ethercount = DIFFZERO( New->regis.ethercount, Prev->regis.ethercount );
  Delta->regis.etherbytes = DIFFZERO( New->regis.etherbytes, Prev->regis.etherbytes );
  Delta->regis.plipcount = DIFFZERO( New->regis.plipcount, Prev->regis.plipcount );
  Delta->regis.plipbytes = DIFFZERO( New->regis.plipbytes, Prev->regis.plipbytes );
  Delta->regis.slipcount = DIFFZERO( New->regis.slipcount, Prev->regis.slipcount );
  Delta->regis.slipbytes = DIFFZERO( New->regis.slipbytes, Prev->regis.slipbytes );
  Delta->regis.pppcount = DIFFZERO( New->regis.pppcount, Prev->regis.pppcount );
  Delta->regis.pppbytes = DIFFZERO( New->regis.pppbytes, Prev->regis.pppbytes );
  Delta->regis.loopcount = DIFFZERO( New->regis.loopcount, Prev->regis.loopcount );
  Delta->regis.loopbytes = DIFFZERO( New->regis.loopbytes, Prev->regis.loopbytes );
  Delta->regis.othercount = DIFFZERO( New->regis.othercount, Prev->regis.othercount );
  Delta->regis.otherbytes = DIFFZERO( New->regis.otherbytes, Prev->regis.otherbytes );
  Delta->regis.aarp = DIFFZERO( New->regis.aarp, Prev->regis.aarp );
  Delta->regis.rtmprd = DIFFZERO( New->regis.rtmprd, Prev->regis.rtmprd );
  Delta->regis.nbp = DIFFZERO( New->regis.nbp, Prev->regis.nbp );
  Delta->regis.atp = DIFFZERO( New->regis.atp, Prev->regis.atp );
  Delta->regis.aep = DIFFZERO( New->regis.aep, Prev->regis.aep );
  Delta->regis.rtmpreq = DIFFZERO( New->regis.rtmpreq, Prev->regis.rtmpreq );
  Delta->regis.zip = DIFFZERO( New->regis.zip, Prev->regis.zip );
  Delta->regis.adsp = DIFFZERO( New->regis.adsp, Prev->regis.adsp );

  Delta->regis.new_ethernet_count = DIFFZERO( New->regis.new_ethernet_count, Prev->regis.new_ethernet_count );
  Delta->regis.unknown_type = DIFFZERO( New->regis.unknown_type, Prev->regis.unknown_type );

  if (options.ip_option)
  {
	Delta->ip_types.count = Delta->ip_protocol_count;
	tally_delta( &New->ip_types, &Prev->ip_types, &Delta->ip_types );
  }

  tally_delta( &New->prot, &Prev->prot, &Delta->prot );

  if (options.tcp_option)
  {
	Delta->tcp_port.count = Delta->tcp_port_ctr;
	tally_delta( &New->tcp_port, &Prev->tcp_port, &Delta->tcp_port );
  }

  if (options.udp_option)
  {
	Delta->udp_port.count = Delta->udp_port_ctr;
	tally_delta( &New->udp_port, &Prev->udp_port, &Delta->udp_port );
  }

  if (options.sap_option)
  {
	Delta->sap_types.count = Delta->sap_count;
	tally_delta( &New->sap_types, &Prev->sap_types, &Delta->sap_types );
  }

}
