/*  tally_init.c is part of Statnet */
/* Statnet is protected under the GNU Public License (GPL2). */
/* Author: Jeroen Baekelandt (jeroenb@igwe.vub.ac.be)       */
/* 21FEB98: Scot E. Wilcoxon (sewilco@fieldday.mn.org)     */

/* This initializes a struct Tally item. */

#include <values.h>
#include "stat.h"

void tally_init ( struct Tally *ts,
	Tally_Type	type_code,
	unsigned int	*count,	/* Array of counters */
	int		c_show_max,
	int		*c_show_list,
	int		c_max,		/* Maximum counter index value */
	char		*c_labels,	/* Array of counter labels */
	unsigned int	*types,	/* Array of types */
	unsigned int	*t_count,	/* Array of type counters */
	unsigned int	*t_values,	/* Array of type values */
	int		t_max,		/* Maximum type counter index value */
	int		t_max_labels,	/* Maximum type label index value */
	char		*t_labels	/* Array of type labels */
)
{
	int	temp_int;

	memset( ts, 0, sizeof(struct Tally) );
	ts->type_code = type_code;

	ts->count = count;
	ts->c_show_max = c_show_max;
	ts->c_show_list = c_show_list;
	ts->c_max = c_max;
	ts->c_labels = c_labels;
	if( c_max > 0 && count != (unsigned int *)0 )
	{
		for (temp_int = 0; temp_int <= c_max; temp_int++)
		{
			ts->count[temp_int] = 0;
		}
	}

	ts->types = types;
	ts->t_count = t_count;
	ts->t_values = t_values;
	ts->t_max = t_max;
	ts->t_max_labels = t_max_labels;
	if( t_max > 0 && types != (unsigned int *)0 )
	{
		for (temp_int = 0; temp_int <= t_max; temp_int++)
		{
			ts->types[temp_int] = 0;
		}
	}
}
