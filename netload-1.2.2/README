(C) 1996 - 2007 Luis Falcon (lfalcon@thymbra.com)
( GPL. Please read LICENSE for more information ).
*****************************************************************************


"netload" displays and monitor the network load for different devices. It uses ncurses to display the information, and a "meter" to show the network load.

It shows the absolute load, relative load, and other statistics, as well as the maximum load on the system so far.

It can send email messages to administrators in case of unusual load in the interface.

You can run multiple sessions in different terminal to check simultaneously several interfaces.


Compiling / Installing
-----------------------
Please read the INSTALL file


Running Netload
----------------
Netload file $HOME/.netloadrc allows you to add any number of network interfaces, modify the alarm settings and your email preferences.


Invoking netload: netload device [-t secs]

where the device is the name of your NIC, ppp, irda,etc.. as shown in ifconfig.
You can find what interfaces are active by running the script "checkint".

The first time that netload is run it generates $HOME/.netloadrc, which should be changed to fit your preferences. This is a sample :


-----------------------

## Configuration file for netload
## (c) 1996-2007 Luis Falcon lfalcon@thymbra.com


## Activate (1) or deactivate (0) the high-load alarm
alarm:1   
## Notify (1) or disable (0) high-load sound notification
sound:0 
## Notify (1) or disable (0) high-load email/SMS notification
email:1
## Period ( in minutes ) high-load email notification
period:30

## Insert and/or your interfaces here, followed by colon and the scale factor.
## one mark is equivalent to x Bytes/sec. There are 60 marks in the meter
ppp0:170
lo:30000
eth0:30000

## Email notification stuff...don't get rid or modify  "From,To and Subject" headers, or sendmail won't work properly

From: admin@foo.bar  
To: hi@laspalmas.buenosaires.net
Subject: High load in your Network 
It seems that there is a high load in one of your interfaces. In the next lines there's a more detailed description of the event.

------------------------------------------

Don't tell your life in this message :) The actual valuable information will be appended to it automatically. 

** The maximum .netloadrc file is 9KB... **

**Alarm parameter:

If the alarm parameter is set to 0, no alarm ( sound or email ) will run.

* sound parameter

If set to 1, depending on the terminal emulation, it will beep or flash ( is beep is not supported, as in the case of some x terminals ).

* email parameter:
If set to 1, it will sent an email with the data fed beginning at the header "From" until the end of the file


Notes on parameter "period".
-----------------------------
The "period" parameter specifies the duration between sending emails when the load is high. I have placed this param as a precautionary measure against flooding your recipient's email. It's safe not to make it smaller that 15 minutes....

----------

Bailing out : Just press control+c .


ABOUT THE SCALE FACTOR OF THE DEVICES
-------------------------------------

"High load" it a pretty subjective concept. It depends on the speed itself of the interface, the "average" load of the network, etc...
To make this concept useful, .netloadrc allows you to create and modify the scale factor of your devices to fit your preferences. The scale factor is calculated in the following way.

For each device, think what could be "a very high load":
That "very high load" represents the end of the meter.
The meter has 60 marks

Example : doing a ping -f to a host using eth0, it gave me an average of 2 Mbytes per second. To calculate the scale factor ( that is, a single mark on the meter ) I divided 2 Mbytes / 60 marks and got approximately 30 Kbytes per mark.

In the case of the interface ppp, I had in mind a regular 56Kbps modem and a Internet connection. I hardly got more than 6-7Kbytes per second, so I decided to make an upper limit of 10KB. Applying the formula, it gives me a scale of 170Bytes per mark.

Again, these are very generic settings that most users will have to change them to make netload useful.

The lever is divided into low, medium and high load :

Low (white) 	: marks <= 20 
Medium ( green ): 20 < marks <= 40 
High ( red )	: marks > 40


------------------------

Last but not least....  expect bugs :) .... this - like every - program needs to be tested among many people. Your suggestions and fixes are the best way of making open source better.


OK, that's about it. Hope you find it useful !

Please send comments, suggestions and bugs to lfalcon@thymbra.com


Luis.
